﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreemptConfiguration.Common
{
    public class GlobalConst
    {
        public const string ConstOasisBP = "Oasis - BP";
        public const string ConstOasisChevron = "Oasis - Chevron";
        public const string ConstOasisParent = "Oasis";
        public const string ConstBP = "BP";
        public const string ConstChevron = "Chevron";
        public const string ConstAppliedInformation = "Applied Information";
        public const string ConstDrillBusiness = "JEF Drill and Blast";
        public const string ConstGlance = "Glance";
        public const string ConstJEF2 = "JEF2";
        public const string ConstDisplay2 = "Display2";
        public const string ConstDisplay3 = "Display3";
        public const string ConstDrill = "Drill";
        public const string ConstDisplay = "Display";
        public const string ConstITSDisplay = "ITSDisplay";//Track Job
        public const string ConstIconDirectory = "Icons";
        public const string Industrial2 = "Industrial2";
        public const string Industrial = "Industrial";
        public const string Industrial3 = "Industrial3";
        public const string Industrial4 = "Industrial4";
        public const string ITSDisplay = "ITSDisplay";    //Trac Device
        public const string ITSDisplayType = "ITSDisplayType";
        public const string SchoolBeaconType = "SchoolBeacon";   //Code Added for School Beacon
        public const string SchoolBeaconClass = "SchoolBeacon";
        public const string ParkingVMSType = "ParkingVMS";
        public const string BaySensorType = "BaySensor";
        public const string ConstRtms = "RTMS";

        public const string ConstImageExtension = ".png";
        public const string ConstImageVariableSignExtension = ".gif";
        public const string ConstOffline = "o";
        public const string ConstMarginalNormal = "mn";
        public const string ConstNormalNone = "nn";
        public const string ConstMarginalLow = "ml";
        public const string ConstNormalLow = "nl";
        public const string ConstMarginalHigh = "mh";
        public const string ConstNormalHigh = "nh";
        public const string ConstMarginalCritical = "mc";
        public const string ConstNormalCritical = "nc";
        public const int ConstPageTime = 15;
        public const string morning = "Morning Shift";
        public const string Night = "Night Shift";
        public const string day = "Day";
        public const string _14day = "14 Day Average";
        public const string BeaconOn = "Beacon ON";
        public const string BeaconOff = "Beacon OFF";
        public const string BeaconSchedule = "Beacon Schedule";
        public const string Offline = "offline";
        public const string CommFail = "Comms Fail";
        public const string Online = "Online";
        public const string Marginal = "Marginal";
        public const string RedirectToLogOn = "<h2><a href='../Account/Login'>Back to Login</a></h2>";
        public const string ErrorPath = "\\ErrorLog";


        //source and messages for ScheduleMissmatch

        public const string ScenarioSource = "Activate Scenario Button";
        public const string ScheduleSaveSource = "Schedule Save Button";
        public const string SchedulePublishSource = "Schedule Publish Button";
        public const string HolidyaSource = "Holidays Publish Button";
        public const string level2ScheduleSource = "Schedule Radio Button";
        public const string level2OnSource = "On Radio Button";
        public const string level2OffSource = "Off Radio Button";


        public const string ScenarioMessage = "Override is Active (Scenario)";
        public const string ScheduleSaveMessage = "Saved Schedule Mismatch";
        public const string SchedulePublishMessage = "Schedule Mismatch";
        public const string HolidyaMessage = "Holidays Mismatch";
        public const string level2ScheduleMessage = "Override (Schedule) is Active";
        public const string level2OnMessage = "Override (On) is Active";
        public const string level2OffMessage = "Override (Off) is Active";


        public const string ConstDeviceStatusPlaying = "Playing";
        public const string ConstDeviceStatusOff = "Off";
        public const string ConstDeviceStatusCommsFails = "Comm Fail";
        public const string ConstDeviceStatusError = "Error";
        public const string ConstDelayPublishSource = "Program Delay Button";
        public const string ConstDelayPublishMessage = "Holiday Mismatch";

    }

}
    
