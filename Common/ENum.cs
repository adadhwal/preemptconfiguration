﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PreemptConfiguration.Common
{
    public enum DirectionEnum
    {
        DontCare, Towards, Away
    }
}