﻿using Microsoft.ApplicationBlocks.Data;
using PreemptConfiguration.Interfaces;
using PreemptConfiguration.Models;
using PreemptConfiguration.Services;
using PreemptConfiguration.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace PreemptConfiguration.Common
{
    public static class CommonHelper
    {
        public static string DBConnectionString = ConfigurationManager.ConnectionStrings["GlanceDBCS"].ConnectionString;
        public static string SelectDeviceTypes = ConfigurationManager.AppSettings["SelectDeviceTypes"];
        public static string XMLLocation = ConfigurationManager.AppSettings["XMLLocation"];
        public static string PDFLocation = ConfigurationManager.AppSettings["PDFLocation"];
        public static double DefaultLatitude = Convert.ToDouble(ConfigurationManager.AppSettings["DefaultLatitude"]);
        public static double DefaultLongitude = Convert.ToDouble(ConfigurationManager.AppSettings["DefaultLongitude"]);
        public static string GreenDotIcon = Convert.ToString(ConfigurationManager.AppSettings["GreenDotIcon"]);

        #region Temporary Static Cache
        private static GoogleObject _object = new GoogleObject();
        public static GoogleObject GoogleMapObject
        {
            get { return _object; }
            set { _object = value; }
        }
       // public static Device _currentDeviceObject = new Device(0, "", 0, "", 0, "", CommonHelper.DefaultLatitude, CommonHelper.DefaultLongitude);//Empty Device Object

        public static IRule currentRuleObject { get; set; }
        public static Device currentDeviceObject { get; set; }
        #endregion


        public static int CountExtensionRules(int DeviceID)
        {
            int CountExtensionRule = 0;
            XmlDocument myXmlDocument = new XmlDocument();
            string xmlDirPath = string.Empty;
            xmlDirPath = CommonHelper.XMLLocation + DeviceID.ToString() + "\\";
            string docFileName = string.Empty;
            docFileName = xmlDirPath + ("preempt.xml");
            if (File.Exists(docFileName))
            {
                myXmlDocument.Load(docFileName);
                XmlNode MultiVehiclenodes = myXmlDocument.SelectSingleNode("Preempt/Rule/MultiVehicleRows");
                if (MultiVehiclenodes != null)
                {
                    CountExtensionRule = MultiVehiclenodes.ChildNodes.Count;
                }
            }
            return CountExtensionRule;
        }
        public static void WriteLog(string logFile, string messageValue, string innerException, string text, string controllerName, string parentView, int DeviceId = 0)
        {
            if (!Directory.Exists(logFile))
            {
                Directory.CreateDirectory(logFile);
            }
            string CurrDate = "\\PreemptLogFile-" + DateTime.Now.ToString("MM-dd-yyyy");
            string logFileName = logFile + CurrDate + ".txt";
            string uname = Convert.ToString(HttpContext.Current.Session["UserName"]);

            if (File.Exists(logFileName))
            {
                using (StreamWriter writer = new StreamWriter(logFileName, true))
                {
                    writer.WriteLine(Environment.NewLine);
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine("Username: " + uname);
                    writer.WriteLine("Error On: " + controllerName);
                    writer.WriteLine("Funciton Name:" + parentView);
                    writer.WriteLine("Message:" + messageValue);
                    writer.WriteLine("DeviceId:" + Convert.ToString(DeviceId));
                    writer.WriteLine("Inner Exception:" + innerException);
                    writer.WriteLine("Stack Trace:" + text);
                    writer.WriteLine(text);
                    writer.WriteLine("======================================================================================================");

                }
            }
            else
            {
                StreamWriter writer = File.CreateText(logFileName);
                writer.WriteLine(Environment.NewLine);
                writer.WriteLine(DateTime.Now.ToString());
                writer.WriteLine("Username: " + uname);
                writer.WriteLine("Error On: " + controllerName);
                writer.WriteLine("Funciton Name:" + parentView);
                writer.WriteLine("Message:" + messageValue);
                writer.WriteLine("Inner Exception:" + innerException);
                writer.WriteLine("Stack Trace:" + text);
                writer.WriteLine(text);
                writer.WriteLine("======================================================================================================");

                writer.Close();


            }
        }
        public static Device GetPreemptUserDevices(int Userid, int deviceID = -1)
        {
            Device obj = new Device(0, "", 0, "", 0, "", CommonHelper.DefaultLatitude, CommonHelper.DefaultLongitude);//Empty Device Object
            List<Device> list = new List<Device>();
            string selectDeviceTypes = CommonHelper.SelectDeviceTypes;
            string XMLLocation = CommonHelper.XMLLocation;
            //Create main directory if not exist
            if (!Directory.Exists(XMLLocation))
            {
                Directory.CreateDirectory(XMLLocation);
            }
            try
            {
                if (Userid > 0)
                {
                    DataSet ds = SqlHelper.ExecuteDataset(CommonHelper.DBConnectionString, CommandType.StoredProcedure, "Usp_GetPreemptUserDevicesNew_AD_Testing",
                        new SqlParameter("@UserId", Userid),
                        new SqlParameter("@DeviceTypes", selectDeviceTypes),
                        new SqlParameter("@DeviceID", deviceID)
                        );
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string _latitude = Convert.ToString(dr["StatusLatitude"]);
                            string _longitude = Convert.ToString(dr["StatusLongitude"]);
                            double Latitude = ((string.IsNullOrEmpty(_latitude) || _latitude == "90000001") ? CommonHelper.DefaultLatitude : Convert.ToDouble(_latitude));
                            double Longitude = ((string.IsNullOrEmpty(_longitude) || _longitude == "180000001") ? CommonHelper.DefaultLongitude : Convert.ToDouble(_longitude));
                            var glancedevices = new Device(Convert.ToInt32(dr["DeviceID"]), Convert.ToString(dr["DeviceName"]), Convert.ToInt32(dr["DeviceTypeID"]), Convert.ToString(dr["DeviceTypeName"]), Convert.ToInt32(dr["BusinessID"]), Convert.ToString(dr["BusinessName"]), Latitude, Longitude);
                            string xmlDirPath = XMLLocation + Convert.ToInt32(dr["DeviceID"]) + "\\";
                            if (!Directory.Exists(xmlDirPath))
                            {
                                Directory.CreateDirectory(xmlDirPath);
                                XMLFileManager _obj = new XMLFileManager();
                                var result = _obj.CreatePreemptXML(xmlDirPath, glancedevices, "Central", "");
                            }
                            list.Add(glancedevices);
                        }
                    }
                }
                else
                {
                    //log off 
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            obj.DeviceList = list;
            return obj;
        }

        public static List<SelectListItem> BindDeviceRuleHistoryDropdown(int deviceId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "--Select--", Value = "-1" });
            string Xmlfilepath = CommonHelper.XMLLocation;
            string CombineHistoryPath = Xmlfilepath + deviceId + "\\history\\";
            DirectoryInfo fileinfo = new DirectoryInfo(CombineHistoryPath);
            if (Directory.Exists(CombineHistoryPath))
            {
                FileInfo[] Files = fileinfo.GetFiles("*.xml");
                if (fileinfo.GetFiles().Length > 0)
                {
                    foreach (FileInfo file in Files)
                        items.Add(new SelectListItem() { Text = Path.GetFileNameWithoutExtension(file.Name), Value = Path.GetFileNameWithoutExtension(file.Name) });
                }
            }
            return items;
        }

        public static DeviceFixGPSLat FixGPSDeviceStatus(int deviceid)
        {
            DeviceFixGPSLat devicefixgpslatlong = new DeviceFixGPSLat();
            if (deviceid > 0)
            {
                DataSet ds = SqlHelper.ExecuteDataset(DBConnectionString, CommandType.StoredProcedure, "Usp_DeviceStatusForPreempt", new SqlParameter("@deviceId", deviceid));
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        devicefixgpslatlong.Latitude = (!string.IsNullOrEmpty(Convert.ToString(dr["StatusLatitude"])) ? 0 : Convert.ToDouble(dr["StatusLatitude"]));
                        devicefixgpslatlong.Longitude = (!string.IsNullOrEmpty(Convert.ToString(dr["StatusLongitude"])) ? 0 : Convert.ToDouble(dr["StatusLongitude"]));
                    }
                }
            }
            return devicefixgpslatlong;
        }

        #region Already added Static Methods
        public static int GetDirection(DirectionEnum preemptDirection)
        {
            int result = 0;

            switch (preemptDirection)
            {
                case DirectionEnum.DontCare:
                    result = 0;
                    break;
                case DirectionEnum.Towards:
                    result = 1;
                    break;
                case DirectionEnum.Away:
                    result = 2;
                    break;
                default:
                    break;
            }

            return result;
        }
        public static DirectionEnum SetDirection(int preemptDirection)
        {
            DirectionEnum result = DirectionEnum.DontCare;

            switch (preemptDirection)
            {
                case 0:
                    result = DirectionEnum.DontCare;
                    break;
                case 1:
                    result = DirectionEnum.Towards;
                    break;
                case 2:
                    result = DirectionEnum.Away;
                    break;
                default:
                    break;
            }

            return result;
        }
        public static int ConvertMetersToFeet(int distanceMETERS)
        {
            double FT = distanceMETERS / 0.304800610;
            return (int)Math.Round(FT, 0, MidpointRounding.AwayFromZero);
        }
        public static int ConvertFeetToMeters(int distanceFEET)
        {
            double M = distanceFEET * 0.304800610;
            return (int)Math.Round(M, 0, MidpointRounding.AwayFromZero);
        }
        public static int ConvertMilesToKilometers(int speedMPH)
        {
            double KPH = speedMPH * 1.609344;
            return (int)Math.Round(KPH, 0, MidpointRounding.AwayFromZero);
        }
        public static int ConvertKilometersToMiles(int speedKPH)
        {
            double MPH = speedKPH * 0.621371192;
            return (int)Math.Round(MPH, 0, MidpointRounding.AwayFromZero);
        }
        public static XmlDocument ToXmlDocument(this XDocument xDocument)
        {
            var xmlDocument = new XmlDocument();
            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }
        public static int PreDirection(int PreDir)
        {
            int result = 0;
            if (Convert.ToInt32(DirectionEnum.Away) == PreDir)
            {
                result = 0;
            }
            else if (Convert.ToInt32(DirectionEnum.DontCare) == PreDir)
            {
                result = 1;
            }
            else if (Convert.ToInt32(DirectionEnum.Towards) == PreDir)
            {
                result = 2;
            }
            return result;
        }
        #endregion

        public static void Errorlog(string msg)
        {
            string logfilepath = HttpContext.Current.Server.MapPath("~/Log.txt");
            FileStream objFilestream = new FileStream(string.Format("{0}", logfilepath), FileMode.Append, FileAccess.Write);
            StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
            objStreamWriter.WriteLine(msg);
            objStreamWriter.WriteLine("======================================================================================================");
            objStreamWriter.Close();
            objFilestream.Close();
        }

        public static string RenderViewToString(Controller controller, string viewName, object model)
        {
            var context = controller.ControllerContext;
            if (string.IsNullOrEmpty(viewName))
                viewName = context.RouteData.GetRequiredString("action");

            var viewData = new ViewDataDictionary(model);

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                var viewContext = new ViewContext(context, viewResult.View, viewData, new TempDataDictionary(), sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        #region Map Functions
        public static string GetColor(int ruleNo)
        {
            string col = "#0000FF";

            if (ruleNo == 1)
            { col = "#0080FF"; }
            else if (ruleNo == 2)
            { col = "#008000"; }
            else if (ruleNo == 3)
            { col = "#FFFF00"; }
            else if (ruleNo == 4)
            { col = "#FF9933"; }
            else if (ruleNo == 5)
            { col = "#FF66CC"; }
            else if (ruleNo == 6)
            { col = "#663300"; }
            else if (ruleNo == 7)
            { col = "#3D9ADE"; }
            else if (ruleNo == 8)
            { col = "#65CC32"; }
            else if (ruleNo == 9)
            { col = "#FFFF99"; }
            else if (ruleNo == 10)
            { col = "#FFCC00"; }
            else if (ruleNo == 11)
            { col = "#FFCCFF"; }
            else if (ruleNo == 12)
            { col = "#CC9900"; }
            else if (ruleNo == 13)
            { col = "#99CCFF"; }
            else if (ruleNo == 14)
            { col = "#009900"; }
            else if (ruleNo == 15)
            { col = "#FFDB9D"; }
            else if (ruleNo == 16)
            { col = "#FFCC65"; }
            else if (ruleNo == 17)
            { col = "#9999FF"; }
            else if (ruleNo == 18)
            { col = "#990000"; }
            else if (ruleNo == 19)
            { col = "#003299"; }
            else if (ruleNo == 20)
            { col = "#99FF65"; }
            else if (ruleNo == 21)
            { col = "#B4E2FF"; }
            else if (ruleNo == 22)
            { col = "#CCCCFF"; }
            else if (ruleNo == 23)
            { col = "#FF3200"; }
            else if (ruleNo == 24)
            { col = "#91AACC"; }
            else if (ruleNo == 25)
            { col = "#0083D7"; }
            else if (ruleNo == 26)
            { col = "#CCFFCC"; }
            else if (ruleNo == 27)
            { col = "#6565CC"; }
            else if (ruleNo == 28)
            { col = "#9999CC"; }
            else if (ruleNo == 29)
            { col = "#656599"; }
            else if (ruleNo == 30)
            { col = "#323265"; }
            else if (ruleNo == 31)
            { col = "#0099FF"; }
            else if (ruleNo == 32)
            { col = "#DEFFFF"; }

            //if (ruleNo == 1)
            //{ col = "#0080FF"; }    //#0000FF = blue
            //else if (ruleNo == 2)
            //{ col = "#008000"; }    //#008000 = green
            //else if (ruleNo == 3)
            //{ col = "#FFFF00"; }    //#FFFF00 = yellow
            //else if (ruleNo == 4)
            //{ col = "#FF9933"; }    //#FF0000 (red = orange
            //else if (ruleNo == 5)
            //{ col = "#FF66CC"; }    //#808080 (gray) = pink
            //else if (ruleNo == 6)
            //{ col = "#663300"; }    //#FFFFFF (white) = brown 

            return col;
        }
        #endregion

        public static Boolean ReadContentFromXML(XmlDocument myXmlDocument, ref Device dr)
        {
            bool result = false;
            try
            {
                XmlNode ccnode = myXmlDocument.SelectSingleNode("Preempt/CityCode");
                if (ccnode != null) { dr.CityCode = ccnode.InnerText; }

                ccnode = myXmlDocument.SelectSingleNode("Preempt/Edit");
                if (ccnode != null) { dr.EditMode = ccnode.InnerText; }

                ccnode = myXmlDocument.SelectSingleNode("Preempt/FixLatitude");
                if (ccnode != null)
                {
                    dr.FixLatitude = Convert.ToDouble(ccnode.InnerText);
                }

                ccnode = myXmlDocument.SelectSingleNode("Preempt/FixLongitude");
                if (ccnode != null)
                {
                    dr.FixLongitude = Convert.ToDouble(ccnode.InnerText);
                }
                ccnode = myXmlDocument.SelectSingleNode("Preempt/MinSpeed");
                if (ccnode != null)
                {
                    dr.MinSpeed = Convert.ToInt16(ccnode.InnerText);
                }
                else
                {
                    dr.MinSpeed = 0;
                }
                ccnode = myXmlDocument.SelectSingleNode("Preempt/MinSpeedDur");
                if (ccnode != null)
                {
                    dr.MinSpeedDur = Convert.ToInt16(ccnode.InnerText);
                }
                else
                {
                    dr.MinSpeedDur = 0;
                }
                if ((dr.FixLatitude == 90000001 | dr.FixLatitude == 0) && (dr.FixLongitude == 180000001 | dr.FixLongitude == 0))
                {
                    dr.FixGPS = false;

                }
                else
                {
                    dr.FixGPS = true;
                }

                List<IRule> rules = new List<IRule>();

                XmlNode nodeRules = myXmlDocument.SelectSingleNode("Preempt/Rule");

                foreach (XmlNode node in nodeRules)
                {
                    IRule nr = new Models.Rule();
                    if (node.Name != "MultiVehicleRows")
                    {
                        nr.RuleID = Convert.ToInt32(node.SelectSingleNode("RuleID").InnerText);
                        nr.LeftDirection = Convert.ToInt32(node.SelectSingleNode("LeftDir").InnerText);
                        nr.RightDirection = Convert.ToInt32(node.SelectSingleNode("RightDir").InnerText);
                        nr.MinDistance = Convert.ToInt32(node.SelectSingleNode("MinDist").InnerText);
                        nr.MinDistanceFT = CommonHelper.ConvertMetersToFeet(nr.MinDistance);
                        nr.MaxDistance = Convert.ToInt32(node.SelectSingleNode("MaxDist").InnerText);
                        nr.MaxDistanceFT = CommonHelper.ConvertMetersToFeet(nr.MaxDistance);
                        nr.PreemptDistance = Convert.ToInt32(node.SelectSingleNode("PreDist").InnerText);
                        nr.PreemptDistanceFT = CommonHelper.ConvertMetersToFeet(nr.PreemptDistance);
                        nr.PreemptDirection = CommonHelper.SetDirection(Convert.ToInt16(node.SelectSingleNode("PreDir").InnerText));
                        nr.PreemptETA = Convert.ToInt32(node.SelectSingleNode("PreETA").InnerText);
                        nr.MaxDuration = Convert.ToInt32(node.SelectSingleNode("MaxDur").InnerText);
                        nr.ClearDist = Convert.ToInt32(node.SelectSingleNode("ClearDist").InnerText);
                        nr.ClearDistFT = CommonHelper.ConvertMetersToFeet(nr.ClearDist);
                        nr.LeftIndicator = Convert.ToInt32(node.SelectSingleNode("LeftInd").InnerText);
                        nr.RightIndicator = Convert.ToInt32(node.SelectSingleNode("RightInd").InnerText);
                        if (node.SelectSingleNode("MinSpeed") != null)
                        {
                            XmlNode childnode = node.SelectSingleNode("MinSpeed");
                            node.RemoveChild(childnode);
                        }
                        if (node.SelectSingleNode("MinSpeedDur") != null)
                        {
                            XmlNode childnode = node.SelectSingleNode("MinSpeedDur");
                            node.RemoveChild(childnode);
                        }
                        XmlAttribute myattr = node.Attributes["StrtInd"];
                        //if (myattr != null)
                        if (node.SelectSingleNode("StrtInd") != null)
                        {
                            nr.StraightIndicator = Convert.ToInt16(node.SelectSingleNode("StrtInd").InnerText);
                        }
                        else
                        {
                            nr.StraightIndicator = 0;
                        }

                        nr.ETADisableSpeed = Convert.ToInt16(node.SelectSingleNode("ETADisSpd").InnerText);
                        nr.ETADisableSpeedMPH = CommonHelper.ConvertKilometersToMiles(nr.ETADisableSpeed);
                        if (node.SelectSingleNode("ETADisSpdMin") != null)
                            nr.ETADisableSpeedMin = Convert.ToInt16(node.SelectSingleNode("ETADisSpdMin").InnerText);
                        else
                            nr.ETADisableSpeedMin = 0;
                        nr.ETADisableSpeedMinMPH = CommonHelper.ConvertKilometersToMiles(nr.ETADisableSpeedMin);
                        nr.PClass = node.SelectSingleNode("Class").InnerText;
                        nr.PreemptNumber = Convert.ToInt16(node.SelectSingleNode("Output").InnerText);
                        if (node.SelectSingleNode("HeadChk") != null)
                        {
                            nr.HeadChk = Convert.ToInt32(node.SelectSingleNode("HeadChk").InnerText);
                        }
                        else
                        {
                            nr.HeadChk = 0;
                        }
                        nr.RuleChanged = false;

                        // nr.PrepareRulePoints(dr.DeviceID, nr.RuleID, dr.Latitude, dr.Longitude);
                        if ((dr.FixLatitude == 90000001 | dr.FixLatitude == 0) && (dr.FixLongitude == 180000001 | dr.FixLongitude == 0) && dr.FixGPS == false)
                        {
                            nr.PrepareRulePoints(dr.DeviceID, nr.RuleID, dr.Latitude, dr.Longitude);
                        }
                        else
                        {
                            nr.PrepareRulePoints(dr.DeviceID, nr.RuleID, dr.FixLatitude, dr.FixLongitude);
                        }
                        rules.Add(nr);
                    }
                }
                dr.Rules = rules;
                result = true;

                XmlNode MultiVehiclenodes = myXmlDocument.SelectSingleNode("Preempt/Rule/MultiVehicleRows");
                dr.MVechile = new List<ExtensionRules>();
                int Extensioncount = 0;
                if (MultiVehiclenodes != null)
                    foreach (XmlNode nodes in MultiVehiclenodes)
                    {
                        Extensioncount++;
                        ExtensionRules _extensionRules = new ExtensionRules();
                        _extensionRules.mvID = Extensioncount.ToString();
                        _extensionRules.mvRule = nodes.SelectSingleNode("mvRule").InnerText;
                        _extensionRules.mvClass = nodes.SelectSingleNode("mvClass").InnerText;
                        _extensionRules.mvMin = nodes.SelectSingleNode("mvMin").InnerText;
                        _extensionRules.mvMax = nodes.SelectSingleNode("mvMax").InnerText;
                        _extensionRules.mvProt = nodes.SelectSingleNode("mvProt").InnerText;
                        _extensionRules.mvOut = nodes.SelectSingleNode("mvOut").InnerText;
                        string MvOut = nodes.SelectSingleNode("mvOut").InnerText;
                        string[] splitVal = MvOut.ToString().Split(',');
                        if (splitVal.Length > 1)
                        {
                            _extensionRules.mvProtOutput = splitVal[0];
                            _extensionRules.mvOut = splitVal[1];
                        }
                        dr.MVechile.Add(_extensionRules);
                    }
                
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public static bool SavePreemptXML(Device device,string _xmlPath, bool saveAsHistory = false, string nameText = "")
        {
            string xmlDirPath = "";
            string docFileName = "";
            XMLFileManager obj = new XMLFileManager();
            if (saveAsHistory)
            {
                xmlDirPath = _xmlPath + device.DeviceID + "\\History\\";
                docFileName = xmlDirPath + (nameText + ".xml");
            }
            else
            {
                xmlDirPath = _xmlPath + device.DeviceID + "\\";
                docFileName = xmlDirPath + ("preempt.xml");
            }

            bool result = false;
            if (File.Exists(docFileName))
            {
                try
                {
                    File.Delete(docFileName);
                    if (obj.CreatePreemptXML(xmlDirPath, device, "Central", nameText))
                    {
                        result = true;
                        //log.Info("Saved to ..\" + device.DeviceID + "\preempt.xml for " + device.DeviceName);
                    }
                }
                catch (Exception ex)
                {
                    //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
                }
            }
            else
            {
                if (saveAsHistory)
                {
                    if (!Directory.Exists(xmlDirPath))
                    {
                        Directory.CreateDirectory(xmlDirPath);
                    }

                }
                if (obj.CreatePreemptXML(xmlDirPath, device, "Central", nameText))
                {
                    result = true;
                    //log.Info("Saved to ..\" + device.DeviceID + "\preempt.xml for " + device.DeviceName);
                }
            }

            return result;
        }

        //public static void TCIPNSBSEND(int tcpdeviceID)
        //{
        //    try
        //    {
        //        var serverIP = ConfigurationManager.AppSettings["TCP_NSB_IP"].ToString();
        //        var DeviceId = tcpdeviceID;
        //        string ProtocolData = TCPIP_DeviceProtocol.DeviceProtocol.BuildData("R");
        //        Exception ex = TCPIP_NSB_Message.MessageSender.Send(DeviceId, ProtocolData);
        //        if (ex != null)
        //        {
        //        }

        //    }

        //    catch (Exception ex)
        //    {
        //        Utility.WriteLog(AppDomain.CurrentDomain.BaseDirectory + GlobalConst.ErrorPath, ex.Message.ToString(), Convert.ToString(ex.InnerException), Convert.ToString(ex.StackTrace), MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        //Response.Redirect("~/Error.aspx");
        //    }
        //}
    }
}