﻿using PreemptConfiguration.Interfaces;
using PreemptConfiguration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PreemptConfiguration.Common
{
    public static class XMLHelper
    {
        public static void LoadRuleDetail(Device currentDevice, string deviceName, int ruleid, bool displayMetric, ref GoogleObject googleMapObject)
        {
            googleMapObject.CenterPoint = new GooglePoint("Center", currentDevice.Latitude, currentDevice.Longitude);
            GooglePoint _startPoint = new GooglePoint();
            _startPoint.ID = currentDevice.DeviceID.ToString();
            _startPoint.Latitude = currentDevice.Latitude;
            _startPoint.Longitude = currentDevice.Longitude;
            _startPoint.IconImage = "../Content/icons/junction.png";
            _startPoint.InfoHTML = currentDevice.DeviceName + " (" + currentDevice.Rules.Count.ToString() + " Rules)";
            _startPoint.ToolTip = _startPoint.InfoHTML;
            googleMapObject.Points.Add(_startPoint);
            googleMapObject.RecenterMap = true;

            GooglePoint RulePt;
            GooglePolygon PG;
            string minDistanceLeft = string.Empty;
            string maxDistanceLeft = string.Empty;
            string minDistanceRight = string.Empty;
            string maxDistanceRight = string.Empty;

            #region Iterate over Rules
            foreach (var r in currentDevice.Rules)
            {
                //Create Polygon-------------------------------------------------------
                PG = new GooglePolygon();
                PG.ID = "PG" + r.RuleID.ToString();
                //Give Hex code for line color
                PG.FillColor = CommonHelper.GetColor(r.RuleID);
                PG.FillOpacity = 0.4;
                PG.StrokeColor = PG.FillColor;
                PG.StrokeOpacity = 1;
                PG.StrokeWeight = 2;


                if (currentDevice.Rules.Count == 1 || r.RuleID == ruleid)
                {
                    #region if there is ony one Rule OR Selected Single Rule 
                    //Create Polylines between points device and Max left; points device and Max right
                    GooglePoint GPD = new GooglePoint();
                    GPD.ID = "GPD";

                    //GPD.Latitude = currentDevice.Latitude;         //taking lat long from preempt
                    //GPD.Longitude = currentDevice.Longitude;
                    GPD.Latitude = currentDevice.FixLatitude;
                    GPD.Longitude = currentDevice.FixLongitude;
                    GooglePolyline PLLeft = new GooglePolyline();
                    PLLeft.ID = "PLDevice";
                    //Give Hex code for line color
                    PLLeft.ColorCode = "#A2A251"; //PG.FillColor; #D28E00
                                                  //Specify width for line
                    PLLeft.Width = 1;
                    PLLeft.Geodesic = true;
                    PLLeft.Points.Add(GPD);

                    GooglePolyline PLRight = new GooglePolyline();
                    PLRight.ID = "PRDevice";
                    //Give Hex code for line color
                    PLRight.ColorCode = "#A2A251"; //PG.FillColor;
                                                   //Specify width for line
                    PLRight.Width = 1;
                    PLRight.Geodesic = true;
                    PLRight.Points.Add(GPD);
                    //Polygon = (minLatLeft,minLonLeft)-> (latLeft, lonLeft) -> (latRight, lonRight) -> (minLatRight,minLonRight)
                    //Min Distance point-------------------------------------------------
                    RulePt = new GooglePoint();
                    //RulePt.ID = this._currentDevice.DeviceID + ":LM=" + r.RuleID.ToString();
                    RulePt.ID = r.LeftMin.PointID;
                    RulePt.Latitude = r.LeftMin.Latitude;
                    RulePt.Longitude = r.LeftMin.Longitude;
                    RulePt.IconImage = "../Content/icons/" + r.RuleID.ToString() + r.LeftMin.PointIcon + ".png";
                    //RulePt.InfoHTML = this._currentDevice.DeviceName + " (Left Direction: " + r.LeftDirection.ToString() + " Min Distance: " + r.MinDistance.ToString() + "m)";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Left Direction: " + r.LeftMin.BearingFromStart.ToString() + "° Min Distance: " + r.LeftMin.DistanceFromStart.ToString() + "m)";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Left Direction: " + r.LeftMin.BearingFromStart.ToString() + "° Min Distance: " + minDistanceLeft;
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Left Direction: " + r.LeftMin.BearingFromStart.ToString() + "° Min Distance: " + 0;//txtMinDis.Text; Need To Test Here txtMinDis.Text + "f";
                    RulePt.ToolTip = RulePt.InfoHTML;
                    RulePt.Draggable = true;
                    googleMapObject.Points.Add(RulePt);
                    PG.Points.Add(RulePt);

                    //Max Distance points-------------------------------------------------
                    RulePt = new GooglePoint();
                    RulePt.ID = r.LeftMax.PointID;
                    RulePt.Latitude = r.LeftMax.Latitude;
                    RulePt.Longitude = r.LeftMax.Longitude;
                    RulePt.IconImage = "../Content/icons/" + r.RuleID.ToString() + r.LeftMax.PointIcon + ".png";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Left Direction: " + r.LeftMax.BearingFromStart.ToString() + "° Max Distance: " + 0;//txtMinDis.Text; Need To Test Here txtMaxDis.Text + "f";
                    RulePt.ToolTip = RulePt.InfoHTML;
                    RulePt.Draggable = true;
                    googleMapObject.Points.Add(RulePt);
                    PG.Points.Add(RulePt);
                    //Also add this point to polyline
                    PLLeft.Points.Add(RulePt);

                    RulePt = new GooglePoint();
                    //RulePt.ID = this._currentDevice.DeviceID + ":RX=" + r.RuleID.ToString(); 
                    RulePt.ID = r.RightMax.PointID;
                    RulePt.Latitude = r.RightMax.Latitude;
                    RulePt.Longitude = r.RightMax.Longitude;
                    RulePt.IconImage = "../Content/icons/" + r.RuleID.ToString() + r.RightMax.PointIcon + ".png";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMax.BearingFromStart.ToString() + "° Max Distance: " + r.RightMax.DistanceFromStart.ToString() + "m)";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMax.BearingFromStart.ToString() + "° Max Distance: " + maxDistanceRight;
                    // RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMax.BearingFromStart.ToString() + "° Max Distance: " + 0;//txtMinDis.Text; Need To Test Here txtMaxDis.Text + "f";
                    RulePt.ToolTip = RulePt.InfoHTML;
                    RulePt.Draggable = true;
                    googleMapObject.Points.Add(RulePt);
                    PG.Points.Add(RulePt);
                    //Also add this point to polyline
                    PLRight.Points.Add(RulePt);

                    //Min Distance point-------------------------------------------------
                    RulePt = new GooglePoint();
                    //RulePt.ID = this._currentDevice.DeviceID + ":RM=" + r.RuleID.ToString(); 
                    RulePt.ID = r.RightMin.PointID;
                    RulePt.Latitude = r.RightMin.Latitude;
                    RulePt.Longitude = r.RightMin.Longitude;
                    RulePt.IconImage = "../Content/icons/" + r.RuleID.ToString() + r.RightMin.PointIcon + ".png";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMin.BearingFromStart.ToString() + "° Min Distance: " + r.RightMin.DistanceFromStart.ToString() + "m)";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMin.BearingFromStart.ToString() + "° Min Distance: " + minDistanceRight;
                    // RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMin.BearingFromStart.ToString() + "° Min Distance: " + 0;//txtMinDis.Text; Need To Test Here
                    RulePt.ToolTip = RulePt.InfoHTML; //
                    RulePt.Draggable = true;
                    googleMapObject.Points.Add(RulePt);
                    PG.Points.Add(RulePt);

                    //Add polygon to GoogleMap object
                    googleMapObject.Polygons.Add(PG);

                    //Add left polyline to GoogleMap object
                    googleMapObject.Polylines.Add(PLLeft);
                    //Add right polyline to GoogleMap object
                    googleMapObject.Polylines.Add(PLRight);
                    #endregion
                }
                else
                {
                    //Polygon = (minLatLeft,minLonLeft)-> (latLeft, lonLeft) -> (latRight, lonRight) -> (minLatRight,minLonRight)
                    //Min Distance point-------------------------------------------------
                    RulePt = new GooglePoint();
                    //RulePt.ID = this._currentDevice.DeviceID + ":LM=" + r.RuleID.ToString(); 
                    RulePt.ID = r.LeftMin.PointID;
                    RulePt.Latitude = r.LeftMin.Latitude;
                    RulePt.Longitude = r.LeftMin.Longitude;
                    //RulePt.IconImage = "icons/" + r.RuleID.ToString() + r.LeftMin.PointIcon + ".png";
                    if (displayMetric)
                    {
                        minDistanceLeft = r.LeftMin.DistanceFromStart.ToString() + "m)";
                        maxDistanceLeft = r.LeftMax.DistanceFromStart.ToString() + "m)";

                        minDistanceRight = r.RightMin.DistanceFromStart.ToString() + "m)";
                        maxDistanceRight = r.RightMax.DistanceFromStart.ToString() + "m)";
                    }
                    else
                    {
                        minDistanceLeft = r.LeftMin.DistanceFromStartFeet.ToString() + "f)";
                        maxDistanceLeft = r.LeftMax.DistanceFromStartFeet.ToString() + "f)";

                        minDistanceRight = r.RightMin.DistanceFromStartFeet.ToString() + "f)";
                        maxDistanceRight = r.RightMax.DistanceFromStartFeet.ToString() + "f)";
                    }
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Left Direction: " + r.LeftMin.BearingFromStart.ToString() + "° Min Distance: " + minDistanceLeft;
                    RulePt.ToolTip = RulePt.InfoHTML;
                    //GoogleMapForASPNet1.googleMapObject.Points.Add(RulePt);
                    PG.Points.Add(RulePt);

                    //Max Distance points-------------------------------------------------
                    RulePt = new GooglePoint();
                    //RulePt.ID = this._currentDevice.DeviceID + ":LX=" + r.RuleID.ToString();
                    RulePt.ID = r.LeftMax.PointID;
                    RulePt.Latitude = r.LeftMax.Latitude;
                    RulePt.Longitude = r.LeftMax.Longitude;
                    //RulePt.IconImage = "icons/" + r.RuleID.ToString() + r.LeftMax.PointIcon + ".png";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Left Direction: " + r.LeftMax.BearingFromStart.ToString() + "° Max Distance: " + r.LeftMax.DistanceFromStart.ToString() + "m)";
                    // RulePt.InfoHTML = currentDevice.DeviceName + " (Left Direction: " + r.LeftMax.BearingFromStart.ToString() + "° Max Distance: " + maxDistanceLeft;
                    RulePt.ToolTip = RulePt.InfoHTML;
                    //GoogleMapForASPNet1.googleMapObject.Points.Add(RulePt);
                    PG.Points.Add(RulePt);

                    RulePt = new GooglePoint();
                    //RulePt.ID = this._currentDevice.DeviceID + ":RX=" + r.RuleID.ToString(); 
                    RulePt.ID = r.RightMax.PointID;
                    RulePt.Latitude = r.RightMax.Latitude;
                    RulePt.Longitude = r.RightMax.Longitude;
                    //RulePt.IconImage = "icons/" + r.RuleID.ToString() + r.RightMax.PointIcon + ".png";
                    //RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMax.BearingFromStart.ToString() + "° Max Distance: " + r.RightMax.DistanceFromStart.ToString() + "m)";
                    // RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMax.BearingFromStart.ToString() + "° Max Distance: " + maxDistanceRight;
                    RulePt.ToolTip = RulePt.InfoHTML;
                    //GoogleMapForASPNet1.googleMapObject.Points.Add(RulePt);
                    PG.Points.Add(RulePt);

                    //Min Distance point-------------------------------------------------
                    RulePt = new GooglePoint();
                    //RulePt.ID = this._currentDevice.DeviceID + ":RM=" + r.RuleID.ToString(); 
                    RulePt.ID = r.RightMin.PointID;
                    RulePt.Latitude = r.RightMin.Latitude;
                    RulePt.Longitude = r.RightMin.Longitude;
                    //RulePt.IconImage = "icons/" + r.RuleID.ToString() + r.RightMin.PointIcon + ".png";
                    // RulePt.InfoHTML = currentDevice.DeviceName + " (Right Direction: " + r.RightMin.BearingFromStart.ToString() + "° Min Distance: " + minDistanceRight;
                    RulePt.ToolTip = RulePt.InfoHTML; //
                                                      //GoogleMapForASPNet1.googleMapObject.Points.Add(RulePt);
                    PG.Points.Add(RulePt);

                    //Add polygon to GoogleMap object
                    googleMapObject.Polygons.Add(PG);
                }
            }
            #endregion
            if (currentDevice.FixGPS)
            {
                string txtFixLat = (currentDevice.FixLatitude != 0 && currentDevice.FixLatitude != 90000001) ? currentDevice.FixLatitude.ToString() : currentDevice.FixLatitude.ToString();
                string txtFixLong = (currentDevice.FixLongitude != 0 && currentDevice.FixLongitude != 180000001) ? currentDevice.FixLongitude.ToString() : currentDevice.FixLongitude.ToString();
                //lockButton.ImageUrl = "~/icons/lock.png";

                RulePt = new GooglePoint();
                RulePt.ID = currentDevice.DeviceName;
                RulePt.Latitude = Convert.ToDouble(txtFixLat);
                RulePt.Longitude = Convert.ToDouble(txtFixLong);
                RulePt.IconImage = "../../Content/icons/f.png";
                RulePt.ToolTip = "Click lock button to drag.";
                RulePt.Draggable = false;
                googleMapObject.Points.Add(RulePt);
            }
            CommonHelper.GoogleMapObject = googleMapObject;
            //Session["GOOGLE_MAP_OBJECT"] = googleMapObject;
            //Session["GOOGLE_MAP_OBJECT_OLD"] = new GoogleObject(googleMapObject);
        }
    }
}