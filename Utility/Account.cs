﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Web.Security;
using System.Globalization;
using System.Text;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace PreemptConfiguration.Utility
{
    /// <summary>
    /// Summary description for Account
    /// </summary>
    public class Account
    {

        public Int32 UserId { get; set; }
        public Int32 BusinessID { get; set; }
        public string UserName { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public Int32 UserRole { get; set; }
        public string Email { get; set; }


        public bool AuthenticateUser(string _username, string _password)
        {
            bool result = false;
            string CS = ConfigurationManager.ConnectionStrings["GlanceDBCS"].ConnectionString;
            try
            {
                  
            }
            catch (Exception)
            {

                throw;
            }


            try
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "select top 1 * from tblUsers Where UserName = @UserName";
                    cmd.Parameters.AddWithValue("@UserName", _username);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                if (VerifyHash(_password, "SHA1", rdr["UserPassword"].ToString()))
                                {
                                    UserId = Convert.ToInt32(rdr["UserID"]);
                                    BusinessID = Convert.ToInt32(rdr["BusinessID"]);
                                    UserName = Convert.ToString(rdr["UserName"]);
                                    UserFirstName = Convert.ToString(rdr["UserFirstName"]);
                                    UserLastName = Convert.ToString(rdr["UserLastName"]);
                                    UserRole = Convert.ToInt32(rdr["UserRole"]);
                                    Email = Convert.ToString(rdr["email"]);
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                }
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        public bool RedirectExistUser(string _usernid, ref string username)
        {
            bool result = false;
            string CS = ConfigurationManager.ConnectionStrings["GlanceDBCS"].ConnectionString;

            try
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "select top 1 * from tblUsers Where UserID = @UserID";
                    cmd.Parameters.AddWithValue("@UserID", _usernid);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                UserId = Convert.ToInt32(rdr["UserID"]);
                                //BusinessID = Convert.ToInt32(rdr["BusinessID"]);
                                username = Convert.ToString(rdr["UserName"]);
                                //UserFirstName = Convert.ToString(rdr["UserFirstName"]);
                                //UserLastName = Convert.ToString(rdr["UserLastName"]);
                                //UserRole = Convert.ToInt32(rdr["UserRole"]);
                                //Email = Convert.ToString(rdr["email"]);
                            }
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public static Boolean VerifyHash(string plain, string hashAlgorithm, string hashValue)
        {
            // Convert base64-encoded hash value into a byte array.
            byte[] hashWithSaltBytes = Convert.FromBase64String(hashValue);

            // We must know size of hash (without salt).
            int hashSizeInBits, hashSizeInBytes;

            // Make sure that hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Size of hash is based on the specified algorithm.
            switch (hashAlgorithm.ToUpper(new CultureInfo("en-US", false)))
            {
                case "SHA1":
                    hashSizeInBits = 160;
                    break;

                case "SHA256":
                    hashSizeInBits = 256;
                    break;

                case "SHA384":
                    hashSizeInBits = 384;
                    break;

                case "SHA512":
                    hashSizeInBits = 512;
                    break;

                default: // Must be MD5
                    hashSizeInBits = 128;
                    break;
            }

            // Convert size of hash from bits to bytes.
            hashSizeInBytes = hashSizeInBits / 8;

            // Make sure that the specified hash value is long enough.
            if (hashWithSaltBytes.Length < hashSizeInBytes)
                return false;

            // Allocate array to hold original salt bytes retrieved from hash.
            byte[] saltBytes = new byte[hashWithSaltBytes.Length -
                                        hashSizeInBytes];

            // Copy salt from the end of the hash to the new array.
            for (int i = 0; i < saltBytes.Length; i++)
                saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];

            // Compute a new hash string.
            string expectedHashString =
                        ComputeHash(plain, hashAlgorithm, saltBytes);

            // If the computed hash matches the specified hash,
            // the plain text value must be correct.

            return (hashValue == expectedHashString);
        }

        public static string ComputeHash(string plain, string hashAlgorithm, byte[] saltValue)
        {
            RNGCryptoServiceProvider rng = null;
            HashAlgorithm hash = null;
            try
            {
                // If salt is not specified, generate it on the fly.
                if (saltValue == null)
                {
                    // Define min and max salt sizes.
                    int minSaltSize = 4;
                    int maxSaltSize = 8;

                    // Generate a random number for the size of the salt.
                    Random random = new Random();
                    int saltSize = random.Next(minSaltSize, maxSaltSize);

                    // Allocate a byte array, which will hold the salt.
                    saltValue = new byte[saltSize];

                    // Initialize a random number generator.
                    rng = new RNGCryptoServiceProvider();

                    // Fill the salt with cryptographically strong byte values.
                    rng.GetNonZeroBytes(saltValue);
                }

                // Convert plain text into a byte array.
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plain);

                // Allocate array, which will hold plain text and salt.
                byte[] plainTextWithSaltBytes =
                        new byte[plainTextBytes.Length + saltValue.Length];

                // Copy plain text bytes into resulting array.
                for (int i = 0; i < plainTextBytes.Length; i++)
                    plainTextWithSaltBytes[i] = plainTextBytes[i];

                // Append salt bytes to the resulting array.
                for (int i = 0; i < saltValue.Length; i++)
                    plainTextWithSaltBytes[plainTextBytes.Length + i] = saltValue[i];



                // Make sure hashing algorithm name is specified.
                if (hashAlgorithm == null)
                    hashAlgorithm = "";

                // Initialize appropriate hashing algorithm class.
                switch (hashAlgorithm.ToUpper(new CultureInfo("en-US", false)))
                {
                    case "SHA1":
                        hash = new SHA1Managed();
                        break;

                    case "SHA256":
                        hash = new SHA256Managed();
                        break;

                    case "SHA384":
                        hash = new SHA384Managed();
                        break;

                    case "SHA512":
                        hash = new SHA512Managed();
                        break;

                    default:
                        hash = new MD5CryptoServiceProvider();
                        break;
                }

                // Compute hash value of our plain text with appended salt.
                byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);



                // Create array which will hold hash and original salt bytes.
                byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                    saltValue.Length];

                // Copy hash bytes into resulting array.
                for (int i = 0; i < hashBytes.Length; i++)
                    hashWithSaltBytes[i] = hashBytes[i];

                // Append salt bytes to the result.
                for (int i = 0; i < saltValue.Length; i++)
                    hashWithSaltBytes[hashBytes.Length + i] = saltValue[i];

                // Convert result into a base64-encoded string.
                string hashValue = Convert.ToBase64String(hashWithSaltBytes);



                // Return the result.
                return hashValue;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                //     rng.Dispose();
                hash.Dispose();
            }
        }
    }
}