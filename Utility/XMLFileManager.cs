﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;
using System.Web;
using System.Xml.Linq;
using PreemptConfiguration.Interfaces;
using PreemptConfiguration.Common;
using PreemptConfiguration.Models;
using Rule = PreemptConfiguration.Models.Rule;

namespace PreemptConfiguration.Utility
{

    public class XMLFileManager
    {
        public static string LoadXmlFileName { get; set; }

        string _xmlPath = string.Empty;
        XmlWriterSettings _settings = new XmlWriterSettings();
        int _epochSeconds = 0;
        public RuleModel r { get; set; } = new RuleModel();

       
        public Boolean CreatePreemptXML(string xmlDirPath, Device dr, string editMode, string nameText = "")
        {
            bool result = false;
            try
            {
                this._epochSeconds = SecondsSinceEpoch();
                var filename = string.Empty;
                if (string.IsNullOrEmpty(nameText))
                {
                    filename = "preempt.xml";
                }
                else
                {
                    if (!nameText.EndsWith(".xml"))
                    {
                        filename = nameText + ".xml";
                    }
                    else
                    {
                        filename = nameText;
                    }

                }

                using (XmlWriter writer = XmlWriter.Create(xmlDirPath + (filename), this._settings))
                {
                    int rowCount = 0;

                    writer.WriteStartDocument();
                    writer.WriteStartElement("Preempt");    //START 
                    writer.WriteElementString("DeviceID", dr.DeviceID.ToString());
                    writer.WriteElementString("TimeStamp", this._epochSeconds.ToString());
                    writer.WriteElementString("CityCode", dr.CityCode);
                    writer.WriteElementString("Edit", editMode);
                    writer.WriteElementString("FixLatitude", dr.FixLatitude.ToString());
                    writer.WriteElementString("FixLongitude", dr.FixLongitude.ToString());
                    writer.WriteElementString("MinSpeed", dr.MinSpeed.ToString());
                    writer.WriteElementString("MinSpeedDur", dr.MinSpeedDur.ToString());
                    writer.WriteStartElement("Rule");       //ITEMS

                    foreach (IRule r in dr.Rules)
                    {
                        writer.WriteStartElement("RuleRow");    //ROW 
                        rowCount += 1;
                        writer.WriteElementString("RuleID", rowCount.ToString());
                        writer.WriteElementString("LeftDir", r.LeftDirection.ToString());
                        writer.WriteElementString("RightDir", r.RightDirection.ToString());
                        writer.WriteElementString("MinDist", r.MinDistance.ToString());
                        writer.WriteElementString("MaxDist", r.MaxDistance.ToString());
                        writer.WriteElementString("PreDist", r.PreemptDistance.ToString());
                        writer.WriteElementString("PreDir", CommonHelper.GetDirection(r.PreemptDirection).ToString());
                        writer.WriteElementString("PreETA", r.PreemptETA.ToString());
                        writer.WriteElementString("MaxDur", r.MaxDuration.ToString());
                        writer.WriteElementString("ClearDist", r.ClearDist.ToString());
                        writer.WriteElementString("LeftInd", r.LeftIndicator.ToString());
                        writer.WriteElementString("RightInd", r.RightIndicator.ToString());
                        writer.WriteElementString("StrtInd", r.StraightIndicator.ToString());
                        writer.WriteElementString("ETADisSpd", r.ETADisableSpeed.ToString());
                        writer.WriteElementString("ETADisSpdMin", r.ETADisableSpeedMin.ToString());
                        writer.WriteElementString("Class", r.PClass);
                        writer.WriteElementString("Output", r.PreemptNumber.ToString());
                        writer.WriteElementString("HeadChk", r.HeadChk.ToString());
                        writer.WriteEndElement();
                    }
                    // writer.WriteEndElement();
                    //END ITEMS

                    // Write the xml tag Extension rules
                    if (dr.MVechile != null)
                        if (dr.MVechile.Count > 0)
                        {
                            dr.MVechile = dr.MVechile.OrderBy(x => x.mvRule).ToList();
                            writer.WriteStartElement("MultiVehicleRows");
                            int Extensioncount = 0;
                            foreach (var item in dr.MVechile)
                            {
                                Extensioncount++;
                                writer.WriteStartElement("MultiVehicle");
                                writer.WriteElementString("mvID", Extensioncount.ToString());
                                writer.WriteElementString("mvRule", item.mvRule.ToString());
                                writer.WriteElementString("mvClass", item.mvClass.ToString());
                                writer.WriteElementString("mvMin", item.mvMin.ToString());
                                writer.WriteElementString("mvMax", item.mvMax.ToString());
                                writer.WriteElementString("mvProt", item.mvProt.ToString());
                                if (item.mvProt == "3")
                                {
                                    writer.WriteElementString("mvOut", item.mvProtOutput + "," + item.mvOut);
                                }
                                else
                                {
                                    writer.WriteElementString("mvOut", item.mvOut.ToString());
                                }
                                writer.WriteEndElement();

                            }
                            writer.WriteEndElement();
                        }
                    //END START 
                    //writer.WriteEndDocument();
                    writer.WriteEndDocument();
                    dr.XMLFound = true;

                    result = true;
                }
            }

            catch (Exception ex)
            {
                //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
            }
            return result;
        }

        public  int SecondsSinceEpoch()
        {
            DateTime DATE_TIME_MINIMUM = new DateTime(1970, 1, 1, 0, 0, 0);
            TimeSpan span = DateTime.UtcNow.Subtract(DATE_TIME_MINIMUM);//(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            int secondsSinceEpoch = (int)Math.Floor(span.TotalSeconds);
            return secondsSinceEpoch;
        }

        public Boolean ReadPreemptXML(ref Device dr)
        {
            bool result = false;
            try

            {
                XmlDocument myXmlDocument = new XmlDocument();
                string xmlDirPath = string.Empty;
                if (string.IsNullOrEmpty(this._xmlPath))
                {
                    string XMLLocation = CommonHelper.XMLLocation;
                    xmlDirPath = XMLLocation + dr.DeviceID + "\\";
                }
                else
                {
                    xmlDirPath = this._xmlPath + dr.DeviceID + "\\";
                }
                string docFileName = string.Empty;
                if (!string.IsNullOrEmpty(LoadXmlFileName))
                {
                    string CombineHistoryPath = xmlDirPath + "history\\";
                    docFileName = CombineHistoryPath + (LoadXmlFileName + ".xml");
                    LoadXmlFileName = null;
                }
                else
                {
                    docFileName = xmlDirPath + ("preempt.xml");
                }
                if (File.Exists(docFileName))
                {
                    myXmlDocument.Load(docFileName);
                    result=CommonHelper.ReadContentFromXML(myXmlDocument,ref dr);
                }
            }

            catch (Exception ex)
            {
                //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
            }
            return result;
        }

        

        public string ConvertXmlToHtmlTable(string xml)
        {

            StringBuilder html = new StringBuilder("<table align='center' " + "border='1' class='xmlTable'>\r\n");

            XDocument xDocument = XDocument.Parse(xml);
            XElement root = xDocument.Root;

            var xmlAttributeCollection = root.Elements().Attributes();
            foreach (var ele in root.Elements())
            {
                if (!ele.HasElements)
                {
                    string elename = "";
                    html.Append("<tr>");

                    elename = ele.Name.ToString();

                    if (ele.HasAttributes)
                    {
                        IEnumerable<XAttribute> attribs = ele.Attributes();
                        foreach (XAttribute attrib in attribs)
                            elename += Environment.NewLine + attrib.Name.ToString() +
                              "=" + attrib.Value.ToString();
                    }

                    html.Append("<td>" + elename + "</td>");
                    html.Append("<td>" + ele.Value + "</td>");
                    html.Append("</tr>");
                }
                else
                {
                    foreach (var subEle in ele.Elements())
                    {
                        if (subEle.Name.ToString() != "MultiVehicleRows")
                        {
                            string elename1 = "";
                            html.Append("<tr>");
                            elename1 = subEle.Name.ToString();
                            html.Append("<td colspan=\"2\" style='font-size:15px;font-weight:bold;text-align:center'>" + elename1 + "</td>");
                            html.Append("</tr>");
                        }
                        if (subEle.HasElements)
                        {
                            foreach (var subSubEle in subEle.Elements())
                            {
                                if (!subSubEle.HasElements)
                                {
                                    string elename = "";
                                    html.Append("<tr>");
                                    elename = subSubEle.Name.ToString();

                                    if (subSubEle.HasAttributes)
                                    {
                                        IEnumerable<XAttribute> attribs = subSubEle.Attributes();
                                        foreach (XAttribute attrib in attribs)
                                            elename += Environment.NewLine + attrib.Name.ToString() + "=" + attrib.Value.ToString();
                                    }

                                    html.Append("<td>" + elename + "</td>");
                                    html.Append("<td>" + subSubEle.Value + "</td>");
                                    html.Append("</tr>");

                                }
                                else if (subEle.Name.ToString() == "MultiVehicleRows")
                                {
                                    string elename1 = "";
                                    html.Append("<tr>");
                                    elename1 = subSubEle.Name.ToString();
                                    html.Append("<td colspan=\"2\" style='font-size:15px;font-weight:bold;text-align:center'>" + elename1 + "</td>");
                                    html.Append("</tr>");

                                    foreach (var MultiV in subSubEle.Elements())
                                    {
                                        string elename = "";
                                        html.Append("<tr>");
                                        elename = MultiV.Name.ToString();

                                        if (subSubEle.HasAttributes)
                                        {
                                            IEnumerable<XAttribute> attribs = subSubEle.Attributes();
                                            foreach (XAttribute attrib in attribs)
                                                elename += Environment.NewLine + attrib.Name.ToString() + "=" + attrib.Value.ToString();
                                        }

                                        html.Append("<td>" + elename + "</td>");
                                        html.Append("<td>" + MultiV.Value + "</td>");
                                        html.Append("</tr>");
                                    }

                                }
                            }
                        }
                    }
                }
            }
            html.Append("</table>");
            return html.ToString();

        }

       
    }
}

