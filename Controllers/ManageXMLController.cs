﻿using PreemptConfiguration.Common;
using PreemptConfiguration.Interfaces;
using PreemptConfiguration.Models;
using PreemptConfiguration.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace PreemptConfiguration.Controllers
{
    public class ManageXMLController : Controller
    {
        [HttpPost]
        public JsonResult UploadXMLFile(int deviceID)
        {
            Device dr =  new Device(0, "", 0, "", 0, "", CommonHelper.DefaultLatitude, CommonHelper.DefaultLongitude);//Empty Device Object
            string error_message = "";
            bool result = false;
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileName(file.FileName);
                        if (Path.GetExtension(_FileName) == ".xml")
                        {
                            Device obj = CommonHelper.GetPreemptUserDevices(Convert.ToInt32(Session["UserId"]), deviceID);
                            dr = obj.DeviceList.FirstOrDefault();
                            XMLFileManager _xmlObject = new XMLFileManager();
                            XDocument xDocument = new XDocument();
                            Stream fileStream = null;
                            fileStream = file.InputStream;
                            byte[] byXML = new byte[file.ContentLength];
                            fileStream.Read(byXML, 0, file.ContentLength);
                            fileStream.Position = 0;
                            xDocument = XDocument.Load(fileStream);
                            XmlDocument myXmlDocument = new XmlDocument();
                            myXmlDocument = xDocument.ToXmlDocument();
                            result = CommonHelper.ReadContentFromXML(myXmlDocument, ref dr);
                        }
                        else
                        {
                            error_message = "Only XML file are allowed. Please select a valid XML file";
                        }
                    }
                }
            }
            else
            {
                error_message = "Please select any XML file to load rules";
            }
            return Json(new { error_message = error_message, device_detail = dr }, JsonRequestBehavior.AllowGet);
        }
    }
}
