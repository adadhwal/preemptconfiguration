﻿using PreemptConfiguration.Common;
using PreemptConfiguration.Interfaces;
using PreemptConfiguration.Models;
using PreemptConfiguration.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PreemptConfiguration.Controllers
{
    public class MapUtilityController : Controller
    {
        public void SetZoom(string pID, int pZoomLevel)
        {
            GoogleObject objGoogleNew = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
            GoogleObject objGoogleOld = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT_OLD"];
            objGoogleNew.ZoomLevel = pZoomLevel;
            objGoogleOld.ZoomLevel = pZoomLevel;
        }

        //This method will be used once map centering is complete. This will set RecenterMap flag to false. So next time map will not recenter automatically.
        public void RecenterMapComplete()
        {
            GoogleObject objGoogleNew = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
            GoogleObject objGoogleOld = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT_OLD"];
            objGoogleNew.RecenterMap = false;
            objGoogleOld.RecenterMap = false;
        }
        public JsonResult GetOptimizedGoogleObject()
        {
            GoogleObject objGoogleNew = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
            GoogleObject objGoogleOld = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT_OLD"];
            if (objGoogleOld != null)
            {
                for (int i = 0; i < objGoogleNew.Points.Count; i++)
                {
                    string pointStatus = "";
                    GooglePoint NewPoint = objGoogleNew.Points[i];
                    GooglePoint OldPoint = objGoogleOld.Points[NewPoint.ID];
                    //if old point not found, means this is a new point.
                    if (OldPoint == null)
                    {
                        pointStatus = "N"; //New
                    }
                    else
                    {
                        //If old point found and old not equal to new point, means it's value is changed.
                        if (!OldPoint.Equals(NewPoint))
                        {
                            pointStatus = "C"; //Changed
                        }
                        //Remove found point from old object. This is to reduce iteration in next loop.
                        objGoogleOld.Points.Remove(OldPoint.ID);
                    }
                    if (pointStatus != "")
                    {
                        //If new point is changed, add it in list which is to be sent to client.
                        NewPoint.PointStatus = pointStatus;
                        objGoogleNew.Points.Add(NewPoint);
                    }
                }
                //Loop through rest of old points to mark them as deleted.
                for (int i = 0; i < objGoogleOld.Points.Count; i++)
                {
                    GooglePoint OldPoint = objGoogleOld.Points[i];
                    OldPoint.PointStatus = "D";
                    objGoogleNew.Points.Add(OldPoint);
                }

                //********************************************
                for (int i = 0; i < objGoogleNew.Polylines.Count; i++)
                {
                    string lineStatus = "";
                    GooglePolyline NewLine = objGoogleNew.Polylines[i];
                    GooglePolyline OldLine = objGoogleOld.Polylines[NewLine.ID];
                    //if old point not found, means this is a new point.
                    if (OldLine == null)
                    {
                        lineStatus = "N"; //New
                    }
                    else
                    {
                        //If old point found and old not equal to new point, means it's value is changed.
                        if (!OldLine.Equals(NewLine))
                        {
                            lineStatus = "C"; //Changed
                        }
                        //Remove found point from old object. This is to reduce iteration in next loop.
                        objGoogleOld.Polylines.Remove(OldLine.ID);
                    }
                    if (lineStatus != "")
                    {
                        //If new point is changed, add it in list which is to be sent to client.
                        NewLine.LineStatus = lineStatus;
                        objGoogleNew.Polylines.Add(NewLine);
                    }
                }
                //Loop through rest of old points to mark them as deleted.
                for (int i = 0; i < objGoogleOld.Polylines.Count; i++)
                {
                    GooglePolyline OldPolyline = objGoogleOld.Polylines[i];
                    OldPolyline.LineStatus = "D";
                    objGoogleNew.Polylines.Add(OldPolyline);
                }

                //********************************************
                for (int i = 0; i < objGoogleNew.Polygons.Count; i++)
                {
                    string gonStatus = "";
                    GooglePolygon NewGon = objGoogleNew.Polygons[i];
                    GooglePolygon OldGon = objGoogleOld.Polygons[NewGon.ID];
                    //if old point not found, means this is a new point.
                    if (OldGon == null)
                    {
                        gonStatus = "N"; //New
                    }
                    else
                    {
                        //If old point found and old not equal to new point, means it's value is changed.
                        if (!OldGon.Equals(NewGon))
                        {
                            gonStatus = "C"; //Changed
                        }
                        //Remove found point from old object. This is to reduce iteration in next loop.
                        objGoogleOld.Polygons.Remove(OldGon.ID);
                    }
                    if (gonStatus != "")
                    {
                        //If new point is changed, add it in list which is to be sent to client.
                        NewGon.Status = gonStatus;
                        objGoogleNew.Polygons.Add(NewGon);
                    }
                }
                //Loop through rest of old points to mark them as deleted.
                for (int i = 0; i < objGoogleOld.Polygons.Count; i++)
                {
                    GooglePolygon OldPolygon = objGoogleOld.Polygons[i];
                    OldPolygon.Status = "D";
                    objGoogleNew.Polygons.Add(OldPolygon);
                }
            }

            //Save new Google object state in session variable.
            System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"] = objGoogleNew;
            return Json(objGoogleNew, JsonRequestBehavior.AllowGet);
        }

        
        public JsonResult OnPushpinDrag(string pID, int Zoomlevel, int deviceID, int ruleid, double pLatitude, double pLongitude)
        {
            bool displayMetric = false;
            GoogleObject GoogleMapObject = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
            GoogleMapObject.Points[pID].Latitude = pLatitude;
            GoogleMapObject.Points[pID].Longitude = pLongitude;
            MovePointsInMapObject obj = new MovePointsInMapObject();
            obj.cache_mode = -1;
            bool Ismove = true;

            string dragend_message = "";
            string txtFixLat = "";
            string txtFixLong = "";
            string txtLeftDirect = "";
            string txtRightDirect = "";
            string txtMinDis = "";
            string txtMaxDis = "";

            Device CurrentDevice = CommonHelper.GetPreemptUserDevices(Convert.ToInt32(Session["UserId"]), deviceID).DeviceList.ToList().FirstOrDefault();
            XMLFileManager _xmlObject = new XMLFileManager();
            _xmlObject.ReadPreemptXML(ref CurrentDevice);
            Rule CurrentRule = (Rule)CurrentDevice.Rules.Where(x => x.RuleID == ruleid).ToList().FirstOrDefault();

            GooglePoint pt = GoogleMapObject.Points[pID];
            dragend_message = pt.ToolTip + ": moved to (" + String.Format("{0:N6}", pt.Latitude) + ", " + String.Format("{0:N6}", pt.Longitude) + ")";
            txtFixLat = String.Format("{0:N6}", pt.Latitude);
            txtFixLong = String.Format("{0:N6}", pt.Longitude);

            if (CurrentRule != null)
            {
                int bearing = CurrentRule.GetBearing(pt.Latitude, pt.Longitude);
                int distanceMeters = CurrentRule.GetDistanceInMeters(pt.Latitude, pt.Longitude);
                int distanceFeet = CommonHelper.ConvertMetersToFeet(distanceMeters);
                if (CurrentRule.MaxDistanceFT == distanceFeet || CurrentRule.MinDistanceFT == distanceFeet)
                {
                    //distanceFeet= distanceFeet + 10;
                    Ismove = false;
                    pt = GoogleMapObject.Points[pID];
                }

                if (Ismove == true)
                {
                    GoogleMapObject.ZoomLevel = Zoomlevel;

                    if (pt.IconImage != CommonHelper.GreenDotIcon)
                    {
                        if (pID.Contains("L"))
                        {
                            CurrentRule.LeftDirection = bearing;
                            txtLeftDirect = bearing.ToString();

                        }
                        else if (pID.Contains("R"))
                        {

                            CurrentRule.RightDirection = bearing;
                            txtRightDirect = bearing.ToString();

                        }

                        if (pID.Contains("M"))  //
                        {
                            CurrentRule.MinDistance = distanceMeters;
                            CurrentRule.MinDistanceFT = distanceFeet;
                            txtMinDis = CurrentDevice.FixGPS ? distanceMeters.ToString() : distanceFeet.ToString();
                        }
                        else if (pID.Contains("X"))
                        {
                            CurrentRule.MaxDistance = distanceMeters;
                            CurrentRule.MaxDistanceFT = distanceFeet;
                            txtMaxDis = CurrentDevice.FixGPS ? distanceMeters.ToString() : distanceFeet.ToString();
                        }
                    }
                    dragend_message = pt.ToolTip + ": moved to (" + bearing.ToString() + "°; Distance = " + (CurrentDevice.FixGPS ? distanceMeters.ToString() : distanceFeet.ToString()) + ")";
                    //This method will adjust (or re-order) all the related points after the pin move
                    obj = MoveRelatedPoints(pt, bearing, distanceMeters, CurrentRule, CurrentDevice, obj, CurrentDevice.FixGPS);

                    //update with rule details...
                    txtLeftDirect = CurrentRule.LeftDirection.ToString();
                    txtRightDirect = CurrentRule.RightDirection.ToString();
                    txtMinDis = CurrentDevice.FixGPS ? CurrentRule.MinDistance.ToString() : CurrentRule.MinDistanceFT.ToString();
                    txtMaxDis = CurrentDevice.FixGPS ? CurrentRule.MaxDistance.ToString() : CurrentRule.MaxDistanceFT.ToString();
                    GoogleMapObject.RecenterMap = true;
                    XMLHelper.LoadRuleDetail(CurrentDevice, CurrentDevice.DeviceName, ruleid, displayMetric, ref GoogleMapObject);
                    Session["GOOGLE_MAP_OBJECT"] = GoogleMapObject;
                    //GoogleMapHTML = CommonHelper.RenderViewToString(this, "_GoogleMap", GoogleMapObject);
                }
            }
            return Json(new { dragend_message = dragend_message,  GoogleMapObject = GoogleMapObject }, JsonRequestBehavior.AllowGet);
        }


        public MovePointsInMapObject MoveRelatedPoints(GooglePoint pt, int bearing, int distanceMeters, Rule CurrentRule, Device CurrentDevice, MovePointsInMapObject obj, bool displayMetric = false)
        {
            try
            {

                IPoint point = null;
                bool changeDistance = false;
                bool changeDirection = false;
                int L = 0;
                int R = 0;
                int minDis = 0;
                int maxDis = 0;

                Points points = new Points(CurrentRule);
                //initialise the temp variables
                foreach (IPoint rp in points.RulePoints)
                {
                    if (rp.PointIcon.EndsWith("A"))
                    {
                        L = rp.BearingFromStart;
                        //   L = bearing;
                        minDis = rp.DistanceFromStart;
                    }
                    else if (rp.PointIcon.EndsWith("B"))
                    {
                        maxDis = rp.DistanceFromStart;
                    }
                    else if (rp.PointIcon.EndsWith("C"))
                    {

                        // minDis = rp.DistanceFromStart;
                        maxDis = rp.DistanceFromStart;
                    }
                    else if (rp.PointIcon.EndsWith("D"))
                    {
                        R = rp.BearingFromStart;
                        //  R = bearing;
                        minDis = rp.DistanceFromStart;
                    }
                }

                foreach (IPoint rp in points.RulePoints)
                {
                    //find point in drag operation already moved
                    if (rp.PointID == pt.ID)
                    {
                        //update the point details
                        rp.Latitude = pt.Latitude;
                        rp.Longitude = pt.Longitude;

                        //set booleans of what has changed
                        changeDirection = rp.BearingFromStart != bearing ? true : false;
                        changeDistance = rp.DistanceFromStart != distanceMeters ? true : false;
                        //changeDistance =  true;


                        //assign the possible new values to the point class
                        rp.BearingFromStart = bearing;
                        rp.DistanceFromStart = distanceMeters;

                        //update the GPoint info...
                        if (displayMetric)
                        {
                            if (pt.ID.Contains("LM"))
                            {
                                pt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + bearing.ToString()
                                    + " Min Distance: " + distanceMeters.ToString() + "m)";
                            }
                            else if (pt.ID.Contains("LX"))
                            {
                                pt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + bearing.ToString()
                                    + " Max Distance: " + distanceMeters.ToString() + "m)";
                            }
                            else if (pt.ID.Contains("RM"))
                            {
                                pt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + bearing.ToString()
                                    + " Min Distance: " + distanceMeters.ToString() + "m)";
                            }
                            else if (pt.ID.Contains("RX"))
                            {
                                pt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + bearing.ToString()
                                    + " Max Distance: " + distanceMeters.ToString() + "m)";
                            }
                        }
                        else
                        {
                            if (pt.ID.Contains("LM"))
                            {
                                pt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + bearing.ToString()
                                    + " Min Distance: " + CommonHelper.ConvertMetersToFeet(distanceMeters).ToString() + "ft)";
                            }
                            else if (pt.ID.Contains("LX"))
                            {
                                pt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + bearing.ToString()
                                    + " Max Distance: " + CommonHelper.ConvertMetersToFeet(distanceMeters).ToString() + "ft)";
                            }
                            else if (pt.ID.Contains("RM"))
                            {
                                pt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + bearing.ToString()
                                    + " Min Distance: " + CommonHelper.ConvertMetersToFeet(distanceMeters).ToString() + "ft)";
                            }
                            else if (pt.ID.Contains("RX"))
                            {
                                pt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + bearing.ToString()
                                    + " Max Distance: " + CommonHelper.ConvertMetersToFeet(distanceMeters).ToString() + "ft)";
                            }
                        }
                        pt.ToolTip = pt.InfoHTML;
                        //assign this point to temp object
                        point = rp;

                        break;
                    }
                }

                if (point != null && (changeDirection | changeDistance))
                {
                    GooglePoint gpt = null;
                    int newBearing, newDistance;

                    if (changeDirection)
                    {
                        //find point partner and make matching changes 
                        foreach (IPoint rp in points.RulePoints)
                        {
                            if (rp.PointID == point.BearingPartnerID)
                            {

                                //new lat long
                                newBearing = changeDirection ? bearing : rp.BearingFromStart;

                                newDistance = rp.DistanceFromStart; //in meters
                                rp.ChangePoint(CurrentDevice.FixLatitude, CurrentDevice.FixLongitude, newBearing, newDistance);
                                //initialise the temp variables
                                if (rp.PointIcon.EndsWith("A") | rp.PointIcon.EndsWith("B"))
                                {
                                    L = newBearing;

                                }
                                else if (rp.PointIcon.EndsWith("C") | rp.PointIcon.EndsWith("D"))
                                {
                                    R = newBearing;
                                }
                                //find the existing google point by ID
                                gpt = CommonHelper.GoogleMapObject.Points[rp.PointID];

                                if (gpt != null)
                                {
                                    //update the GPoint info...
                                    if (displayMetric)
                                    {
                                        if (gpt.ID.Contains("LM"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                                + " Min Distance: " + newDistance.ToString() + "m)";

                                        }
                                        else if (gpt.ID.Contains("LX"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                                + " Max Distance: " + newDistance.ToString() + "m)";

                                        }
                                        else if (gpt.ID.Contains("RM"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                                + " Min Distance: " + newDistance.ToString() + "m)";
                                        }
                                        else if (gpt.ID.Contains("RX"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                                + " Max Distance: " + newDistance.ToString() + "m)";
                                        }
                                    }
                                    else
                                    {
                                        if (gpt.ID.Contains("LM"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                                + " Min Distance: " + CommonHelper.ConvertMetersToFeet(newDistance).ToString() + "ft)";

                                        }
                                        else if (gpt.ID.Contains("LX"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                                + " Max Distance: " + CommonHelper.ConvertMetersToFeet(newDistance).ToString() + "ft)";

                                        }
                                        else if (gpt.ID.Contains("RM"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                                + " Min Distance: " + CommonHelper.ConvertMetersToFeet(newDistance).ToString() + "ft)";
                                        }
                                        else if (gpt.ID.Contains("RX"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                                + " Max Distance: " + CommonHelper.ConvertMetersToFeet(newDistance).ToString() + "ft)";
                                        }
                                    }
                                    gpt.ToolTip = gpt.InfoHTML;
                                    //change the GPoint position...
                                    gpt.Latitude = rp.Latitude;
                                    gpt.Longitude = rp.Longitude;
                                    obj.cache_mode = 1;
                                }
                                break;
                            }
                        }
                    }

                    if (changeDistance)
                    {
                        foreach (IPoint rp in points.RulePoints)
                        {
                            if (rp.PointID == point.DistancePartnerID)
                            {
                                //new lat long
                                newBearing = rp.BearingFromStart;
                                // int bearing1 = this._myDR.CurrentRule.GetBearing(rp.Latitude, rp.Longitude);
                                newDistance = changeDistance ? distanceMeters : rp.DistanceFromStart; //in meters

                                //method will update the point details
                                rp.ChangePoint(CurrentDevice.Latitude, CurrentDevice.Longitude, newBearing, newDistance);

                                if (rp.PointIcon.EndsWith("A") | rp.PointIcon.EndsWith("D"))
                                {

                                    minDis = newDistance;

                                }
                                else if (rp.PointIcon.EndsWith("B") | rp.PointIcon.EndsWith("C"))
                                {
                                    maxDis = newDistance;

                                }
                                //find the existing google point by ID
                                gpt = CommonHelper.GoogleMapObject.Points[rp.PointID];

                                if (gpt != null)
                                {
                                    //update the GPoint info...
                                    if (displayMetric)
                                    {
                                        if (gpt.ID.Contains("LM"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                                + " Min Distance: " + newDistance.ToString() + "m)";

                                        }
                                        else if (gpt.ID.Contains("LX"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                                + " Max Distance: " + newDistance.ToString() + "m)";

                                        }
                                        else if (gpt.ID.Contains("RM"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                                + " Min Distance: " + newDistance.ToString() + "m)";
                                        }
                                        else if (gpt.ID.Contains("RX"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                                + " Max Distance: " + newDistance.ToString() + "m)";
                                        }
                                        obj.cache_mode = 1;
                                    }
                                    else
                                    {
                                        if (gpt.ID.Contains("LM"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                                + " Min Distance: " + CommonHelper.ConvertMetersToFeet(newDistance).ToString() + "ft)";
                                            //gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                            //    + " Min Distance: " + txtMinDis.Text + "ft)";


                                        }
                                        else if (gpt.ID.Contains("LX"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Left Direction: " + newBearing.ToString()
                                                + " Max Distance: " + CommonHelper.ConvertMetersToFeet(newDistance).ToString() + "ft)";

                                        }
                                        else if (gpt.ID.Contains("RM"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                                + " Min Distance: " + CommonHelper.ConvertMetersToFeet(newDistance).ToString() + "ft)";
                                            //gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                            //   + " Min Distance: " + txtMinDis.Text + "ft)";
                                        }
                                        else if (gpt.ID.Contains("RX"))
                                        {
                                            gpt.InfoHTML = CurrentDevice.DeviceName + " (Right Direction: " + newBearing.ToString()
                                                + " Max Distance: " + CommonHelper.ConvertMetersToFeet(newDistance).ToString() + "ft)";
                                        }
                                        obj.cache_mode = 1;
                                    }
                                    gpt.ToolTip = gpt.InfoHTML;
                                    //change the GPoint position...
                                    gpt.Latitude = rp.Latitude;
                                    gpt.Longitude = rp.Longitude;
                                }
                                break;
                            }
                        }
                    }

                    if (gpt != null)
                    {
                        //bool goodValue = true;
                        bool bearFlip = false;
                        bool disFlip = false;

                        if (minDis > maxDis)
                        {
                            disFlip = true;
                        }

                        //test bearings
                        if (L < R)
                        {
                            // bearFlip = true;
                            if ((R - L) <= 180)
                            {
                                //then good
                            }
                            else
                            { bearFlip = true; }
                        }
                        else
                        {
                            if (L <= 180) //We know that L > R here
                            {
                                bearFlip = true;    //then swap
                            }

                            //DONE if left and right are both in quadrant 1 or 2

                            else //implicit: Left is in quadrant 3 or 4, right is in any quadrant
                            {
                                if (R > 180)
                                {
                                    bearFlip = true; //then swap
                                }
                                else //R is in first or second quadrant, so we know R is >0 and R < 180
                                {
                                    if (R < ((L + 180) % 360))
                                    {
                                        //then good
                                    }
                                    else
                                    {
                                        bearFlip = true; //then swap
                                    }
                                }
                            }
                        }

                        if (!bearFlip && !disFlip)
                        {
                            IRule nr = new Rule();
                            nr = (IRule)CurrentRule;
                            nr.LeftDirection = L;
                            nr.RightDirection = R;
                            nr.MinDistance = minDis;
                            nr.MinDistanceFT = CommonHelper.ConvertMetersToFeet(nr.MinDistance);
                            nr.MaxDistance = maxDis;
                            nr.MaxDistanceFT = CommonHelper.ConvertMetersToFeet(nr.MaxDistance);

                            //update rule
                            CurrentRule = (Rule)nr;
                            obj.selectnode = "0/" + CurrentDevice.DeviceID.ToString() + "/" + CurrentDevice.DeviceID.ToString() + ":" + CurrentRule.RuleID.ToString();
                        }
                        else
                        {
                            //reorder points
                            IRule nr = new Rule();
                            nr = CurrentRule;

                            if (bearFlip)
                            {
                                foreach (IPoint rp in points.RulePoints)
                                {

                                    if (rp.PointIcon.EndsWith("A"))
                                    {
                                        //LeftMin becomes RightMin A-D
                                        nr.RightMin = rp;
                                        nr.RightMin.PointIcon = "D";
                                        nr.RightMin.PointID = nr.RightMin.PointID.Replace("LM", "RM");
                                        //partner points also swap
                                        nr.RightMin.BearingPartnerID = nr.RightMin.BearingPartnerID.Replace("LX", "RX");
                                        nr.RightMin.DistancePartnerID = nr.RightMin.DistancePartnerID.Replace("RM", "LM");

                                    }
                                    else if (rp.PointIcon.EndsWith("B"))
                                    {
                                        //LeftMax becomes RightMax B-C
                                        nr.RightMax = rp;
                                        nr.RightMax.PointIcon = "C";
                                        nr.RightMax.PointID = nr.RightMax.PointID.Replace("LX", "RX");
                                        //partner points also swap
                                        nr.RightMax.BearingPartnerID = nr.RightMax.BearingPartnerID.Replace("LM", "RM");
                                        nr.RightMax.DistancePartnerID = nr.RightMax.DistancePartnerID.Replace("RX", "LX");
                                    }
                                    else if (rp.PointIcon.EndsWith("C"))
                                    {
                                        //RightMax becomes LeftMax C- B
                                        nr.LeftMax = rp;
                                        nr.LeftMax.PointIcon = "B";
                                        nr.LeftMax.PointID = nr.LeftMax.PointID.Replace("RX", "LX");
                                        //partner points also swap
                                        nr.LeftMax.BearingPartnerID = nr.LeftMax.BearingPartnerID.Replace("RM", "LM");
                                        nr.LeftMax.DistancePartnerID = nr.LeftMax.DistancePartnerID.Replace("LX", "RX");
                                    }

                                    else if (rp.PointIcon.EndsWith("D"))
                                    {
                                        //RightMin becomes LeftMin D - A
                                        nr.LeftMin = rp;
                                        nr.LeftMin.PointIcon = "A";
                                        nr.LeftMin.PointID = nr.LeftMin.PointID.Replace("RM", "LM");
                                        //partner points also swap
                                        nr.LeftMin.BearingPartnerID = nr.LeftMin.BearingPartnerID.Replace("RX", "LX");
                                        nr.LeftMin.DistancePartnerID = nr.LeftMin.DistancePartnerID.Replace("LM", "RM");

                                    }
                                }

                                nr.LeftDirection = R;
                                nr.RightDirection = L;


                            }
                            else
                            {
                                nr.LeftDirection = L;
                                nr.RightDirection = R;
                            }
                            if (disFlip)
                            {
                                if ((L > R) || (L < R))
                                {
                                    foreach (IPoint rp in points.RulePoints)
                                    {
                                        if (rp.PointIcon.EndsWith("A"))
                                        {
                                            //LeftMin becomes LeftMax  A -B
                                            nr.LeftMax = rp;
                                            nr.LeftMax.PointIcon = "B";
                                            nr.LeftMax.PointID = nr.LeftMax.PointID.Replace("LM", "LX");
                                            //partner points also flip
                                            nr.LeftMax.BearingPartnerID = nr.LeftMax.BearingPartnerID.Replace("LX", "LM");
                                            nr.LeftMax.DistancePartnerID = nr.LeftMax.DistancePartnerID.Replace("RM", "RX");
                                        }
                                        else if (rp.PointIcon.EndsWith("B"))
                                        {
                                            //LeftMax becomes LeftMin B - A
                                            nr.LeftMin = rp;
                                            nr.LeftMin.PointIcon = "A";
                                            nr.LeftMin.PointID = nr.LeftMin.PointID.Replace("LX", "LM");
                                            //partner points also flip
                                            nr.LeftMin.BearingPartnerID = nr.LeftMin.BearingPartnerID.Replace("LM", "LX");
                                            nr.LeftMin.DistancePartnerID = nr.LeftMin.DistancePartnerID.Replace("RX", "RM");
                                        }
                                        else if (rp.PointIcon.EndsWith("C"))
                                        {
                                            //RightMax becomes RightMin  C - D
                                            nr.RightMin = rp;
                                            nr.RightMin.PointIcon = "D";
                                            nr.RightMin.PointID = nr.RightMin.PointID.Replace("RX", "RM");
                                            //partner points also flip
                                            nr.RightMin.BearingPartnerID = nr.RightMin.BearingPartnerID.Replace("RM", "RX");
                                            nr.RightMin.DistancePartnerID = nr.RightMin.DistancePartnerID.Replace("LX", "LM");
                                        }

                                        else if (rp.PointIcon.EndsWith("D"))
                                        {
                                            //RightMin becomes RightMax  D - C
                                            nr.RightMax = rp;
                                            nr.RightMax.PointIcon = "C";
                                            nr.RightMax.PointID = nr.RightMax.PointID.Replace("RM", "RX");
                                            //partner points also flip
                                            nr.RightMax.BearingPartnerID = nr.RightMax.BearingPartnerID.Replace("RX", "RM");
                                            nr.RightMax.DistancePartnerID = nr.RightMax.DistancePartnerID.Replace("LM", "LX");
                                        }

                                    }



                                    nr.MinDistance = maxDis;
                                    nr.MinDistanceFT = CommonHelper.ConvertMetersToFeet(nr.MinDistance);
                                    nr.MaxDistance = minDis;
                                    nr.MaxDistanceFT = CommonHelper.ConvertMetersToFeet(nr.MaxDistance);
                                }
                                else
                                {
                                    foreach (IPoint rp in points.RulePoints)
                                    {
                                        if (rp.PointIcon.EndsWith("B"))
                                        {
                                            //LeftMin becomes LeftMax  A -B
                                            nr.LeftMax = rp;
                                            //nr.LeftMax.PointIcon = "B";// commented on 30April2019
                                            nr.LeftMax.PointIcon = "B"; // turn off swapping pinorder by prerna from A to B commented on 30April2019

                                            nr.LeftMax.PointID = nr.LeftMax.PointID.Replace("LM", "LX");

                                            //partner points also flip
                                            nr.LeftMax.BearingPartnerID = nr.LeftMax.BearingPartnerID.Replace("LX", "LM");
                                            nr.LeftMax.DistancePartnerID = nr.LeftMax.DistancePartnerID.Replace("RM", "RX");
                                        }
                                        else if (rp.PointIcon.EndsWith("A"))
                                        {
                                            //LeftMax becomes LeftMin B - A
                                            nr.LeftMin = rp;
                                            //  nr.LeftMin.PointIcon = "A"; commented on 30April2019
                                            nr.LeftMin.PointIcon = "A";  // turn off swapping pinorder by prerna from B to A commented on 30April2019

                                            nr.LeftMin.PointID = nr.LeftMin.PointID.Replace("LX", "LM");
                                            //partner points also flip
                                            nr.LeftMin.BearingPartnerID = nr.LeftMin.BearingPartnerID.Replace("LM", "LX");
                                            nr.LeftMin.DistancePartnerID = nr.LeftMin.DistancePartnerID.Replace("RX", "RM");
                                        }
                                        else if (rp.PointIcon.EndsWith("D"))
                                        {
                                            //RightMax becomes RightMin  C - D
                                            nr.RightMin = rp;
                                            //nr.RightMin.PointIcon = "D";// commented on 30April2019
                                            nr.RightMin.PointIcon = "D";// // turn off swapping pinorder by prerna from C to D commented on 30April2019

                                            nr.RightMin.PointID = nr.RightMin.PointID.Replace("RX", "RM");
                                            //partner points also flip
                                            nr.RightMin.BearingPartnerID = nr.RightMin.BearingPartnerID.Replace("RM", "RX");
                                            nr.RightMin.DistancePartnerID = nr.RightMin.DistancePartnerID.Replace("LX", "LM");
                                        }

                                        else if (rp.PointIcon.EndsWith("C"))
                                        {
                                            //RightMin becomes RightMax  D - C
                                            nr.RightMax = rp;
                                            // nr.RightMax.PointIcon = "C";// commented on 30April2019
                                            nr.RightMax.PointIcon = "C"; // turn off swapping pinorder by prerna from D to C  commented on 30April2019
                                            nr.RightMax.PointID = nr.RightMax.PointID.Replace("RM", "RX");
                                            //partner points also flip
                                            nr.RightMax.BearingPartnerID = nr.RightMax.BearingPartnerID.Replace("RX", "RM");
                                            nr.RightMax.DistancePartnerID = nr.RightMax.DistancePartnerID.Replace("LM", "LX");
                                        }

                                    }

                                    nr.MinDistance = maxDis;
                                    nr.MinDistanceFT = CommonHelper.ConvertMetersToFeet(nr.MinDistance);
                                    nr.MaxDistance = minDis;
                                    nr.MaxDistanceFT = CommonHelper.ConvertMetersToFeet(nr.MaxDistance);
                                }
                            }
                            obj.status_text = "Reorder Rule points";
                            obj.cache_mode = 1;
                            obj.selectnode = "0/" + CurrentDevice.DeviceID.ToString() + "/" + CurrentDevice.DeviceID.ToString() + ":" + CurrentRule.RuleID.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
            }
            return obj;
        }
        public class MovePointsInMapObject
        {
            public int cache_mode { get; set; }
            public string status_text { get; set; }
            public string selectnode { get; set; }
        }
    }
}