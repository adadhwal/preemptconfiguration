﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PreemptConfiguration.Utility;
using PreemptConfiguration.Common;
using PreemptConfiguration.Models;
using Elmah;
using System.Reflection;

namespace PreemptConfiguration.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            GoogleObject _googlemapobject = new GoogleObject();
            Session["GOOGLE_MAP_OBJECT"] = _googlemapobject;
            Session["GOOGLE_MAP_OBJECT_OLD"] = new GoogleObject(_googlemapobject);
            Device obj = new Device(0, "", 0, "", 0, "", CommonHelper.DefaultLatitude, CommonHelper.DefaultLongitude);//Empty Device Object
            try
            {
                if (Session["UserId"] == null)
                    RedirectToAction("login");
                obj = CommonHelper.GetPreemptUserDevices(Convert.ToInt32(Session["UserId"]));
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                CommonHelper.WriteLog(AppDomain.CurrentDomain.BaseDirectory + GlobalConst.ErrorPath, ex.Message.ToString(), Convert.ToString(ex.InnerException), Convert.ToString(ex.StackTrace), MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return View(obj);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string username, string password)
        {
            Account account = new Account();
            if (account.AuthenticateUser(username, password))
            {
                Session["UserName"] = Convert.ToString(account.UserName);
                Session["UserId"] = Convert.ToString(account.UserId);
                Session["StatusUserLogin"] = false;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ErrorMessage = "Username/Password Combination is Incorrect!";
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult Error()
        {
            return View();
        }

        
    }
}