﻿using PreemptConfiguration.Common;
using PreemptConfiguration.Interfaces;
using PreemptConfiguration.Models;
using PreemptConfiguration.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace PreemptConfiguration.Controllers
{
    public class DeviceInfoController : Controller
    {
        // GET: DeviceInfo
        public JsonResult FetchDeviceDetailByID(int deviceid, int ruleid = 0, bool displayMetric = false, bool dynamicRule = false)
        {
            GoogleObject GoogleMapObject = new GoogleObject();
            Session["GOOGLE_MAP_OBJECT"] = GoogleMapObject;
            Session["GOOGLE_MAP_OBJECT_OLD"] = new GoogleObject(GoogleMapObject);
            Device currentDevice;
            string DeviceHTML = "";
            if (dynamicRule)
            {
                GoogleMapObject = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
                currentDevice = CommonHelper.currentDeviceObject;
                IRule newResult = new Models.Rule();
                newResult = currentDevice.Rules.Where(x => x.RuleID == ruleid).FirstOrDefault();
                CommonHelper.currentRuleObject = newResult;
                XMLHelper.LoadRuleDetail(currentDevice, currentDevice.DeviceName, ruleid, displayMetric, ref GoogleMapObject);
                CommonHelper.GoogleMapObject = GoogleMapObject;
            }
            else
            {
                currentDevice = GetDeviceDetailById_RuleID(deviceid, ruleid, 0);
                currentDevice.ActiveDeviceID = deviceid;
                currentDevice.ActiveRuleID = ruleid;
                currentDevice.FixGPS = true;
                if (deviceid > 0)
                {
                    XMLHelper.LoadRuleDetail(currentDevice, currentDevice.DeviceName, ruleid, displayMetric, ref GoogleMapObject);
                    CommonHelper.GoogleMapObject = GoogleMapObject;
                    Session["GOOGLE_MAP_OBJECT"] = new GoogleObject(GoogleMapObject);
                    Session["GOOGLE_MAP_OBJECT_OLD"] = new GoogleObject(GoogleMapObject);
                }
            }
            DeviceHTML = CommonHelper.RenderViewToString(this, "_DeviceInfo", currentDevice);
            return Json(new { result = currentDevice, DeviceDetail = DeviceHTML, GoogleMapObject = GoogleMapObject }, JsonRequestBehavior.AllowGet);


        }
        public Device GetDeviceDetailById_RuleID(int deviceID, int ruleID, int mvid)
        {
            Device obj = CommonHelper.GetPreemptUserDevices(Convert.ToInt32(Session["UserId"]), deviceID);
            Device dr = obj.DeviceList.FirstOrDefault();
            try
            {
                XMLFileManager _xmlObject = new XMLFileManager();
                //CommonHelper.SavePreemptXML(dr, CommonHelper.XMLLocation);
                _xmlObject.ReadPreemptXML(ref dr);
                dr.RuleHistoryList = CommonHelper.BindDeviceRuleHistoryDropdown(deviceID);
            }
            catch (Exception ex)
            {
                //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
            }
            return dr;
        }

        public bool SaveDeviceDetailToXML(int deviceID, bool saveAsHistory = false, string nameText = "")
        {
            Device obj = CommonHelper.GetPreemptUserDevices(Convert.ToInt32(Session["UserId"]), deviceID);
            Device dr = obj.DeviceList.FirstOrDefault();
            bool result = CommonHelper.SavePreemptXML(dr, CommonHelper.XMLLocation);
            return result;
        }

        #region Bottom Buttons Click Events

        [HttpPost]
        public JsonResult btnCopyRuleClick(int deviceID, int ruleid, bool displayMetric = false)
        {
            GoogleObject GoogleMapObject = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
            Device currentDevice = CommonHelper.currentDeviceObject;
            IRule newResult = new Models.Rule();
            newResult = CommonHelper.currentRuleObject;
            newResult = currentDevice.AddRule(ref currentDevice, newResult);
            CommonHelper.currentRuleObject = newResult;//add copied Result to next Rule
            int maxRuleID = currentDevice.Rules.Count;
            XMLHelper.LoadRuleDetail(currentDevice, currentDevice.DeviceName, maxRuleID, displayMetric, ref GoogleMapObject);
            CommonHelper.GoogleMapObject = GoogleMapObject;
            return Json(new { result = currentDevice, GoogleMapObject = GoogleMapObject }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult btnRemoveRuleClick(int deviceID, int ruleid)
        {
            GoogleObject GoogleMapObject = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
            Device currentDevice = CommonHelper.currentDeviceObject;
            currentDevice.Rules.RemoveAll((x) => x.RuleID == ruleid);
            //currentDevice.Rules = currentDevice.Rules.Where(x => x.RuleID != ruleid).ToList();
            currentDevice.ActiveDeviceID = deviceID;
            currentDevice.ActiveRuleID = ruleid;
            XMLHelper.LoadRuleDetail(currentDevice, currentDevice.DeviceName, -1, false, ref GoogleMapObject);
            return Json(new { result = currentDevice, GoogleMapObject = GoogleMapObject }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult btnAddRuleClick(int deviceID, string txtLeftDirect, string txtRightDirect,
            string txtMinDis,
            string txtMaxDis,
            string txtPreDis,
            string txtClearDist,
            string txtETADisableSpeed,
            string txtETADisableSpeedMin,
            string cboDirection,
            string txtPreETA,
            bool chkLeftIndicator,
            bool chkRightIndicator,
            string txtMaxDuration,
            string txtClass,
            string txtPreemptNo,
            bool chkStraight,
            bool HeadChk,
            bool displayMetric)
        {
            GoogleObject GoogleMapObject = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
            GoogleObject objGoogleOld = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT_OLD"];
            Device obj = CommonHelper.GetPreemptUserDevices(Convert.ToInt32(Session["UserId"]), deviceID);
            Device currentDevice = obj.DeviceList.FirstOrDefault();
            XMLFileManager _obj = new XMLFileManager();
            _obj.ReadPreemptXML(ref currentDevice);//Read Existing Rules from XML
            currentDevice.ActiveDeviceID = deviceID;
            string deviceName = currentDevice.DeviceName;
            // Add rule values
            IRule newResult = new Models.Rule();

            newResult.LeftDirection = Convert.ToInt32(txtLeftDirect == "" ? "15" : txtLeftDirect);
            newResult.RightDirection = Convert.ToInt32(txtRightDirect == "" ? "60" : txtRightDirect);
            if (displayMetric)
            {
                newResult.MinDistance = Convert.ToInt32(txtMinDis == "" ? "100" : txtMinDis);
                newResult.MinDistanceFT = CommonHelper.ConvertMetersToFeet(newResult.MinDistance);
                newResult.MaxDistance = Convert.ToInt32(txtMaxDis == "" ? "300" : txtMaxDis);
                newResult.MaxDistanceFT = CommonHelper.ConvertMetersToFeet(newResult.MaxDistance);
                newResult.PreemptDistance = Convert.ToInt32(txtPreDis == "" ? "0" : txtPreDis);
                newResult.PreemptDistanceFT = CommonHelper.ConvertMetersToFeet(newResult.PreemptDistance);
                newResult.ClearDist = Convert.ToInt32(txtClearDist == "" ? "0" : txtClearDist);
                newResult.ClearDistFT = CommonHelper.ConvertMetersToFeet(newResult.ClearDist);
                newResult.ETADisableSpeed = Convert.ToInt32(txtETADisableSpeed == "" ? "0" : txtETADisableSpeed);
                newResult.ETADisableSpeedMPH = CommonHelper.ConvertKilometersToMiles(newResult.ETADisableSpeed);
                newResult.ETADisableSpeedMin = Convert.ToInt32(txtETADisableSpeedMin == "" ? "0" : txtETADisableSpeedMin);
                newResult.ETADisableSpeedMinMPH = CommonHelper.ConvertKilometersToMiles(newResult.ETADisableSpeedMin);
            }
            else
            {
                newResult.MinDistanceFT = Convert.ToInt32(txtMinDis == "" ? "328" : txtMinDis);
                newResult.MinDistance = CommonHelper.ConvertFeetToMeters(newResult.MinDistanceFT);
                newResult.MaxDistanceFT = Convert.ToInt32(txtMaxDis == "" ? "984" : txtMaxDis);
                newResult.MaxDistance = CommonHelper.ConvertFeetToMeters(newResult.MaxDistanceFT);
                newResult.PreemptDistanceFT = Convert.ToInt32(txtPreDis == "" ? "0" : txtPreDis);
                newResult.PreemptDistance = CommonHelper.ConvertFeetToMeters(newResult.PreemptDistanceFT);
                newResult.ClearDistFT = Convert.ToInt32(txtClearDist == "" ? "0" : txtClearDist);
                newResult.ClearDist = CommonHelper.ConvertFeetToMeters(Convert.ToInt32(newResult.ClearDistFT));
                newResult.ETADisableSpeedMPH = Convert.ToInt32(txtETADisableSpeed == "" ? "0" : txtETADisableSpeed);
                newResult.ETADisableSpeed = CommonHelper.ConvertMilesToKilometers(newResult.ETADisableSpeedMPH);
                newResult.ETADisableSpeedMinMPH = Convert.ToInt32(txtETADisableSpeedMin == "" ? "0" : txtETADisableSpeedMin);
                newResult.ETADisableSpeedMin = CommonHelper.ConvertMilesToKilometers(newResult.ETADisableSpeedMinMPH);
            }
            switch (Convert.ToString(cboDirection))
            {
                case "1":
                    newResult.PreemptDirection = DirectionEnum.Towards;
                    break;
                case "2":
                    newResult.PreemptDirection = DirectionEnum.Away;
                    break;
                default:
                    newResult.PreemptDirection = DirectionEnum.DontCare;
                    break;
            }

            newResult.PreemptETA = Convert.ToInt32(txtPreETA == "" ? "0" : txtPreETA);
            newResult.MaxDuration = Convert.ToInt32(txtMaxDuration == "" ? "0" : txtMaxDuration);
            newResult.LeftIndicator = Convert.ToInt32(chkLeftIndicator);
            newResult.RightIndicator = Convert.ToInt32(chkRightIndicator);
            newResult.PClass = txtClass == "" ? "0" : txtClass;
            newResult.PreemptNumber = Convert.ToInt32(txtPreemptNo == "" ? "0" : txtPreemptNo);
            newResult.StraightIndicator = Convert.ToInt32(chkStraight);
            newResult.HeadChk = Convert.ToInt32(HeadChk);
            newResult.RuleChanged = false;
            newResult = currentDevice.AddRule(ref currentDevice, newResult);

            CommonHelper.currentDeviceObject = currentDevice;//set Loaded Device in for Static Rule 
            CommonHelper.currentRuleObject = newResult; //set New Rule in for Static Rule 

            int maxRuleID = currentDevice.Rules.Count;
            XMLHelper.LoadRuleDetail(currentDevice, currentDevice.DeviceName, maxRuleID, displayMetric, ref GoogleMapObject);
            CommonHelper.GoogleMapObject = GoogleMapObject;
            Session["GOOGLE_MAP_OBJECT"] = GoogleMapObject;
            Session["GOOGLE_MAP_OBJECT_OLD"] = new GoogleObject(GoogleMapObject);
            //string GoogleMapHTML = CommonHelper.RenderViewToString(this, "_GoogleMap", GoogleMapObject);
            return Json(new { result = currentDevice, GoogleMapObject = GoogleMapObject }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult btnSaveXMLHistory_Click(int deviceID, string HistoryText,
           string txtCityCode, bool chkFixGPS, string txtFixLat, string txtFixLong, bool HeadChk)
        {
            GoogleObject GoogleMapObject = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT"];
            GoogleObject objGoogleOld = (GoogleObject)System.Web.HttpContext.Current.Session["GOOGLE_MAP_OBJECT_OLD"];
            string status_message = "";
            try
            {
                var filename = HistoryText;
                if (!string.IsNullOrEmpty(filename))
                {
                    filename = filename.Replace(" ", "_");

                    Device obj = CommonHelper.GetPreemptUserDevices(Convert.ToInt32(Session["UserId"]), deviceID);
                    Device currentDevice = obj.DeviceList.FirstOrDefault();
                    GooglePoint pt = new GooglePoint();
                    if (currentDevice != null)
                    {

                        currentDevice.CityCode = txtCityCode;
                        currentDevice.FixGPS = chkFixGPS;
                        currentDevice.FixLatitude = Convert.ToDouble(txtFixLat);
                        currentDevice.FixLongitude = Convert.ToDouble(txtFixLong);
                        currentDevice.HeadChk = Convert.ToInt32(HeadChk);

                        if (SaveDeviceDetailToXML(currentDevice.DeviceID, true, filename))
                        {
                            status_message = "File Saved to following location //" + currentDevice.DeviceID + "//History//" + filename + ".xml";
                        }
                    }
                    pt = GoogleMapObject.Points[currentDevice.DeviceName];

                    if (pt != null)
                    {
                        pt.Draggable = true;
                    }
                }
                else
                {
                    status_message = "Please enter a file name to save as history";
                }
            }
            catch (Exception ex)
            {
                //Utility.WriteLog(AppDomain.CurrentDomain.BaseDirectory + GlobalConst.ErrorPath, ex.Message.ToString(), Convert.ToString(ex.InnerException), Convert.ToString(ex.StackTrace), MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //Response.Redirect("~/Error.aspx");
            }
            return Json(new { status_message = status_message, filename = HistoryText }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 'Refresh Units PCU 36'
        public JsonResult btnRefresh_Click(int deviceID)
        {
            //CommonHelper.TCIPNSBSEND(deveceid);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Print/Download XML
        protected void btnPrintXMLClick(int deviceID, string deviceName)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + deviceName + ".xml");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/xml";
            Response.WriteFile(Path.Combine(Server.MapPath("~/"), CommonHelper.XMLLocation + deviceID + "/preempt.xml"));
            Response.Flush();
            Response.End();

        }
        public void btnPrintPDFClick(int deviceID)
        {
            XMLFileManager _obj = new XMLFileManager();
            XmlDocument doc = new XmlDocument();
            doc.Load(CommonHelper.XMLLocation + deviceID + "/preempt.xml");
            string html = _obj.ConvertXmlToHtmlTable(doc.InnerXml);
            Document document = new Document();
            string fileName = "Device.pdf";
            if (!Directory.Exists(CommonHelper.PDFLocation))
            {
                Directory.CreateDirectory(CommonHelper.PDFLocation);
            }
            PdfWriter.GetInstance(document, new FileStream(CommonHelper.PDFLocation + "\\" + fileName, FileMode.Create));
            document.Open();
            //StyleSheet styles = new StyleSheet();
            //HTMLWorker hw = new HTMLWorker(document);
            //hw.Parse(new StringReader(html));
            document.Close();
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName.ToString());
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.WriteFile(Path.GetFullPath(CommonHelper.PDFLocation + fileName));
            Response.Flush();
            Response.End();
        }
        #endregion
    }

}