﻿using PreemptConfiguration.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreemptConfiguration.Interfaces
{
    public interface IRule
    {
        int RuleID { get; set; }
        int LeftDirection { get; set; }
        int RightDirection { get; set; }
        int MinDistance { get; set; }
        int MinDistanceFT { get; set; }
        int MaxDistance { get; set; }
        int MaxDistanceFT { get; set; }
        IPoint LeftMax { get; set; }
        IPoint LeftMin { get; set; }
        IPoint RightMax { get; set; }
        IPoint RightMin { get; set; }
        int PreemptDistance { get; set; }
        int PreemptDistanceFT { get; set; }
        DirectionEnum PreemptDirection { get; set; }
        int PreemptETA { get; set; }
        int MaxDuration { get; set; }
        int ClearDist { get; set; }
        int ClearDistFT { get; set; }
        int LeftIndicator { get; set; }
        int RightIndicator { get; set; }
        int StraightIndicator { get; set; }
        int ETADisableSpeed { get; set; }
        int ETADisableSpeedMPH { get; set; }
        int ETADisableSpeedMin { get; set; }
        int ETADisableSpeedMinMPH { get; set; }
        string PClass { get; set; }
        int PreemptNumber { get; set; }
        bool RuleChanged { get; set; }
        int HeadChk { get; set; }
        int MinSpeed { get; set; }
        int MinSpeedDur { get; set; }
        //Methods
        void PrepareRulePoints(int deviceID, int ruleID, double startPtLat, double startPtLong);
        int GetBearing(double newLat, double newLong);
        int GetDistanceInMeters(double newLat, double newLong);
        //created new properties MVechile
        int mvID { get; set; }
        int mvRule { get; set; }

        int mvClass { get; set; }

        int mvMin { get; set; }
        int mvMax { get; set; }
        string mvProt { get; set; }
        int mvOut { get; set; }
    }
}
