﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreemptConfiguration.Interfaces
{
    public interface IPoint
    {
        string PointIcon { get; set; }
        string PointID { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        int DistanceFromStart { get; set; }
        int DistanceFromStartFeet { get; set; }
        int BearingFromStart { get; set; }
        string DistancePartnerID { get; set; }
        string BearingPartnerID { get; set; }

        //Methods
        void ChangePoint(double startLat, double startLong, int newBearing, int newDistance);
    }
}
