﻿using PreemptConfiguration.Common;
using PreemptConfiguration.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;


namespace PreemptConfiguration.Models
{
    public class Rule : IRule
    {
        int _deviceID;
        int _ruleID;
        double _latitude;
        double _longitude;

        public void PrepareRulePoints(int deviceID, int ruleID, double startPtLat, double startPtLong)
        {
            this._deviceID = deviceID;
            this._ruleID = ruleID;
            this._latitude = startPtLat;
            this._longitude = startPtLong;

            if ((this.LeftDirection > 0 | this.RightDirection > 0) && this.MaxDistance > 0)
            {
                CalculateEndPoints();
            }
        }
        private void CalculateEndPoints()
        {
            try
            {
                double latStart, latLeft, latRight, lonStart, lonLeft, lonRight, distance, radLefDir, radRightDir;

                latStart = this._latitude * (Math.PI / 180.0);
                lonStart = this._longitude * (Math.PI / 180.0);

                //Max-distance points----------------------------------------------------------------------
                distance = this.MaxDistance; //divide by earth radius km
                distance = distance / (6371.0 * 1000.0); //meters

                radLefDir = this.LeftDirection * (Math.PI / 180.0);
                radRightDir = this.RightDirection * (Math.PI / 180.0);

                latLeft = Math.Asin(Math.Sin(latStart) * Math.Cos(distance) + Math.Cos(latStart) * Math.Sin(distance) * Math.Cos(radLefDir));
                lonLeft = lonStart + Math.Atan2(Math.Sin(radLefDir) * Math.Sin(distance) * Math.Cos(latStart), Math.Cos(distance) - Math.Sin(latStart) * Math.Sin(latLeft));

                latRight = Math.Asin(Math.Sin(latStart) * Math.Cos(distance) + Math.Cos(latStart) * Math.Sin(distance) * Math.Cos(radRightDir)); ;
                lonRight = lonStart + Math.Atan2(Math.Sin(radRightDir) * Math.Sin(distance) * Math.Cos(latStart), Math.Cos(distance) - Math.Sin(latStart) * Math.Sin(latRight)); ;

                this.LeftMax = new Point();
                this.LeftMax.PointIcon = "B";
                this.LeftMax.PointID = this._deviceID + ":LX=" + this._ruleID;
                this.LeftMax.Latitude = latLeft * (180.0 / Math.PI);
                this.LeftMax.Longitude = lonLeft * (180.0 / Math.PI);
                this.LeftMax.DistanceFromStart = this.MaxDistance;
                this.LeftMax.DistanceFromStartFeet = this.MaxDistanceFT;
                this.LeftMax.BearingFromStart = this.LeftDirection;
                //this.LeftMax.PartnerPointID = "A";

                this.RightMax = new Point();
                this.RightMax.PointIcon = "C";
                this.RightMax.PointID = this._deviceID + ":RX=" + this._ruleID;
                this.RightMax.Latitude = latRight * (180.0 / Math.PI); //Then convert radian into degrees
                this.RightMax.Longitude = lonRight * (180.0 / Math.PI);
                this.RightMax.DistanceFromStart = this.MaxDistance;
                this.RightMax.DistanceFromStartFeet = this.MaxDistanceFT;
                this.RightMax.BearingFromStart = this.RightDirection;
                //this.RightMax.PartnerPointID = "D";

                //Now min-distance points-------------------------------------------------------------------
                double minLonLeft, minLonRight, minLatLeft, minLatRight, minDistance;

                minDistance = this.MinDistance; //divide by earth radius km
                minDistance = minDistance / (6371.0 * 1000.0); //meters

                minLatLeft = Math.Asin(Math.Sin(latStart) * Math.Cos(minDistance) + Math.Cos(latStart) * Math.Sin(minDistance) * Math.Cos(radLefDir));
                minLonLeft = lonStart + Math.Atan2(Math.Sin(radLefDir) * Math.Sin(minDistance) * Math.Cos(latStart), Math.Cos(minDistance) - Math.Sin(latStart) * Math.Sin(latLeft));

                minLatRight = Math.Asin(Math.Sin(latStart) * Math.Cos(minDistance) + Math.Cos(latStart) * Math.Sin(minDistance) * Math.Cos(radRightDir));
                minLonRight = lonStart + Math.Atan2(Math.Sin(radRightDir) * Math.Sin(minDistance) * Math.Cos(latStart), Math.Cos(minDistance) - Math.Sin(latStart) * Math.Sin(latRight));

                this.LeftMin = new Point();
                this.LeftMin.PointIcon = "A";
                this.LeftMin.PointID = this._deviceID + ":LM=" + this._ruleID;
                this.LeftMin.Latitude = minLatLeft * (180.0 / Math.PI);
                this.LeftMin.Longitude = minLonLeft * (180.0 / Math.PI);
                this.LeftMin.DistanceFromStart = this.MinDistance;
                this.LeftMin.DistanceFromStartFeet = this.MinDistanceFT;
                this.LeftMin.BearingFromStart = this.LeftDirection;
                //this.LeftMin.PartnerPointID = "B";

                this.RightMin = new Point();
                this.RightMin.PointIcon = "D";
                this.RightMin.PointID = this._deviceID + ":RM=" + this._ruleID;
                this.RightMin.Latitude = minLatRight * (180.0 / Math.PI);
                this.RightMin.Longitude = minLonRight * (180.0 / Math.PI);
                this.RightMin.DistanceFromStart = this.MinDistance;
                this.RightMin.DistanceFromStartFeet = this.MinDistanceFT;
                this.RightMin.BearingFromStart = this.RightDirection;
                //this.RightMin.PartnerPointID = "C";

                this.LeftMax.BearingPartnerID = this.LeftMin.PointID;
                this.RightMax.BearingPartnerID = this.RightMin.PointID;
                this.LeftMin.BearingPartnerID = this.LeftMax.PointID;
                this.RightMin.BearingPartnerID = this.RightMax.PointID;

                this.LeftMax.DistancePartnerID = this.RightMax.PointID;
                this.RightMax.DistancePartnerID = this.LeftMax.PointID;
                this.LeftMin.DistancePartnerID = this.RightMin.PointID;
                this.RightMin.DistancePartnerID = this.LeftMin.PointID;
            }
            catch (Exception ex)
            {
                //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
            }
        }
        private double LatitudeToRadian(double distanceMeters)
        {
            //(distance / 6371000) in metres (mean radius = 6,371km)
            return (Convert.ToDouble((distanceMeters / 1000.0) / 6371.0) * (180.0 / Math.PI));
        }
        private double DegreeToRadian(double bearing)
        {
            //For final bearing, simply take the initial bearing from the end point to the start point 
            //and reverse it (using θ = (θ+180) % 360).
            //double d = bearing * (Math.PI / 180);
            //return (((d + 180) / 360 * 100)); 
            return bearing * (Math.PI / 180.0);
        }
        private double RadianToDegree(double bearing)
        {
            return bearing * (180.0 / Math.PI);
        }
        public int GetDistanceInMeters(double newLat, double newLong)
        {
            int distance = 0;
            try
            {


                //Distance
                //
                //This uses the ‘haversine’ formula to calculate the great-circle distance between two points
                //– that is, the shortest distance over the earth’s surface – giving an ‘as-the-crow-flies’ 
                //distance between the points (ignoring any hills they fly over, of course!).
                //
                //Haversine
                //formula:	a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
                //c = 2 ⋅ atan2( √a, √(1−a) )
                //d = R ⋅ c
                //where	φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
                //note that angles need to be in radians to pass to trig functions!
                //JavaScript:	
                //var R = 6371000; // metres
                //var φ1 = lat1.toRadians();
                //var φ2 = lat2.toRadians();
                //var Δφ = (lat2-lat1).toRadians();
                //var Δλ = (lon2-lon1).toRadians();
                //
                //var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                //        Math.cos(φ1) * Math.cos(φ2) *
                //        Math.sin(Δλ/2) * Math.sin(Δλ/2);
                //var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                //
                //var d = R * c;
                //----------------------------------------------------------------------------------------

                double φ1 = this._latitude * (Math.PI / 180.0);
                double φ2 = newLat * (Math.PI / 180.0);
                double Δφ = (newLat - this._latitude) * (Math.PI / 180.0);
                double Δλ = (newLong - this._longitude) * (Math.PI / 180.0);

                double a = Math.Sin(Δφ / 2) * Math.Sin(Δφ / 2) + Math.Cos(φ1) * Math.Cos(φ2) * Math.Sin(Δλ / 2) * Math.Sin(Δλ / 2);
                double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

                double d = 6371000.0 * c; // metres

                distance = Convert.ToInt32(d);
            }
            catch (Exception ex)
            {
                //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
            }
            return distance;
        }

        /// <summary>
        /// Dummy Method to get Bearing for testing
        /// </summary>
        /// <param name="newLat"></param>
        /// <param name="newLong"></param>
        /// <returns></returns>
        #region Vijay bearing new method
        static double DegreeBearing(
            double lat1, double lon1,
            double lat2, double lon2)
        {
            var dLon = ToRad(lon2 - lon1);
            var dPhi = Math.Log(
                Math.Tan(ToRad(lat2) / 2 + Math.PI / 4) / Math.Tan(ToRad(lat1) / 2 + Math.PI / 4));
            if (Math.Abs(dLon) > Math.PI)
                dLon = dLon > 0 ? -(2 * Math.PI - dLon) : (2 * Math.PI + dLon);
            return ToBearing(Math.Atan2(dLon, dPhi));
        }

        public static double ToRad(double degrees)
        {
            return degrees * (Math.PI / 180);
        }

        public static double ToDegrees(double radians)
        {
            return radians * 180 / Math.PI;
        }

        public static double ToBearing(double radians)
        {
            // convert radians to degrees (as bearing: 0...360)
            return (ToDegrees(radians) + 360) % 360;
        }

        #endregion

        public int GetBearing(double newLat, double newLong)
        {
            int bearing = 0;
            try
            {
                //Bearing
                //
                //In general, your current heading will vary as you follow a great circle path (orthodrome); 
                //the final heading will differ from the initial heading by varying degrees according to distance 
                //and latitude (if you were to go from say 35°N,45°E (≈ Baghdad) to 35°N,135°E (≈ Osaka), 
                //you would start on a heading of 60° and end up on a heading of 120°!).
                //
                //This formula is for the initial bearing (sometimes referred to as forward azimuth) 
                //which if followed in a straight line along a great-circle arc will take you from the 
                //start point to the //end point:1
                //
                // Formula:	θ = atan2( sin Δλ ⋅ cos φ2 , cos φ1 ⋅ sin φ2 − sin φ1 ⋅ cos φ2 ⋅ cos Δλ )
                //JavaScript:
                //(all angles in radians)
                //var y = Math.sin(λ2-λ1) * Math.cos(φ2);
                //var x = Math.cos(φ1)*Math.sin(φ2) -
                //        Math.sin(φ1)*Math.cos(φ2)*Math.cos(λ2-λ1);
                //var brng = Math.atan2(y, x).toDegrees();
                //----------------------------------------------------------------------------------------

                //λ = long; φ = lat; 

                double y;
                double x;
                double brng;


                y = Math.Sin(newLong - this._longitude) * Math.Cos(newLat);
                x = Math.Cos(this._latitude) * Math.Sin(newLat) - Math.Sin(this._latitude) * Math.Cos(newLat) * Math.Cos(newLong - this._longitude);
                brng = (Math.Atan2(y, x)) * (180.0 / Math.PI); //to Degrees

                //Since atan2 returns values in the range -π ... +π (that is, -180° ... +180°), 
                //to normalise the result to a compass bearing (in the range 0° ... 360°, with −ve values 
                //transformed into the range 180° ... 360°), convert to degrees and then use 
                //(θ+360) % 360 where % is (floating point) modulo.
                double g = (brng + 360.0) % 360.0;
                // if(this._longitude>=-100)
                g = 360.0 - g;

                bearing = Convert.ToInt32(Math.Floor(g));
                //  bearing = Convert.ToInt32((g));


                //x = Math.Cos(newLong) * Math.Sin(this._longitude+ newLong);

                //y = Math.Cos(this._latitude) * Math.Sin(this._longitude) -Math.Sin(newLat) * Math.Cos(this._longitude) * Math.Cos(this._longitude + newLong);

                //brng = Math.Atan2(x, y) * (180.0 / Math.PI);
                //double g = (brng + 360.0) % 360.0;
                //g = 360.0 - g;
                //bearing = Convert.ToInt32(brng);
                bearing = Convert.ToInt32(DegreeBearing(this._latitude, this._longitude, newLat, newLong));
            }
            catch (Exception ex)
            {
                //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
            }
            return bearing;
        }
        public void Errorlog(string msg)
        {
            string logfilepath = HttpContext.Current.Server.MapPath("~/Log.txt");
            FileStream objFilestream = new FileStream(string.Format("{0}", logfilepath), FileMode.Append, FileAccess.Write);
            StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
            objStreamWriter.WriteLine(msg);
            objStreamWriter.WriteLine("======================================================================================================");
            objStreamWriter.Close();
            objFilestream.Close();
            HttpContext.Current.Response.Redirect("~/Error.aspx");
        }
        #region IRule Members

        public int RuleID { get; set; }
        public int LeftDirection { get; set; }
        public int RightDirection { get; set; }
        public int MinDistance { get; set; }
        public int MinDistanceFT { get; set; }
        public int MaxDistance { get; set; }
        public int MaxDistanceFT { get; set; }
        public IPoint LeftMax { get; set; }
        public IPoint LeftMin { get; set; }
        public IPoint RightMax { get; set; }
        public IPoint RightMin { get; set; }
        public int PreemptDistance { get; set; }
        public int PreemptDistanceFT { get; set; }
        public DirectionEnum PreemptDirection { get; set; }
        public int PreemptETA { get; set; }
        public int MaxDuration { get; set; }
        public int ClearDist { get; set; }
        public int ClearDistFT { get; set; }
        public int LeftIndicator { get; set; }
        public int RightIndicator { get; set; }
        public int StraightIndicator { get; set; }
        public int ETADisableSpeed { get; set; }
        public int ETADisableSpeedMPH { get; set; }
        public int ETADisableSpeedMin { get; set; }
        public int ETADisableSpeedMinMPH { get; set; }
        public string PClass { get; set; }
        public int PreemptNumber { get; set; }
        public bool RuleChanged { get; set; }
        public int HeadChk { get; set; }
        public int MinSpeed { get; set; }
        public int MinSpeedDur { get; set; }
        ////created new properties MVechile
        public int mvID { get; set; }
        public int mvRule { get; set; }

        public int mvClass { get; set; }

        public int mvMin { get; set; }
        public int mvMax { get; set; }
        public string mvProt { get; set; }
        public int mvOut { get; set; }

        #endregion

    }
}
