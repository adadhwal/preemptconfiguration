﻿using PreemptConfiguration.Common;
using PreemptConfiguration.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace PreemptConfiguration.Models
{
    public class Device 
    {
        public Device(int deviceID, string deviceName, int deviceTypeID, string deviceTypeName, int businessID, string businessName ,
                           double latitude, double longitude )
        {
            this.DeviceID = deviceID;
            this.DeviceName = deviceName;
            this.DeviceTypeID = deviceTypeID;
            this.DeviceTypeName = deviceTypeName;
            this.BusinessID = businessID;
            this.BusinessName = businessName;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.FixLongitude = CommonHelper.DefaultLongitude;
            this.FixLatitude = CommonHelper.DefaultLatitude;

            this.Rules = new List<IRule>();
            this.CityCode = "City";
            this.MinSpeed = 0;
            this.MinSpeedDur = 0;
            this.XMLFound = false; 
            this.MVechile = new List<ExtensionRules>();
            this.DeviceList = new List<Device>();
            this.RuleHistoryList = new List<SelectListItem>();
        }
     
        #region Device Members
        public int ActiveDeviceID { get; set; }
        public int ActiveRuleID { get; set; }
        public int DeviceID { get; set; }
        public string DeviceName { get; private set; }
        public int DeviceTypeID { get; set; }
        public string DeviceTypeName { get; private set; }
        public string CityCode { get; set; }
        public string EditMode { get; set; }
        public int BusinessID { get; private set; }
        public string BusinessName { get; private set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool FixGPS { get; set; }
        public double FixLatitude { get; set; }
        public double FixLongitude { get; set; }
        public List<IRule> Rules { get; set; }
        public bool XMLFound { get; set; }
        public bool DeviceChanged { get; set; }

        public int HeadChk { get; set; }
        public int MinSpeed { get; set; }
        public int MinSpeedDur { get; set; }
        public bool FixMap { get; set; }
        public List<ExtensionRules> MVechile { get; set; }
        public string mvID { get; set; }
        public string mvRule { get; set; }

        public string mvClass { get; set; }

        public string mvMin { get; set; }
        public string mvMax { get; set; }
        public string mvProt { get; set; }
        public string mvOut { get; set; }
        public List<Device> DeviceList { get; set; }
        public List<SelectListItem> RuleHistoryList { get; set; }
        #endregion

        public List<IRule> Newrules(List<IRule> newrule)
        {
            int deviceID = Convert.ToInt32(HttpContext.Current.Cache["DeviceID"]);
            string xmlPathLocation = ConfigurationManager.AppSettings["XMLLocation"].ToString() + "/" + deviceID + "/preempt.xml";
            XDocument xmlDoc = XDocument.Load(xmlPathLocation);
            var RulesData = xmlDoc.Descendants("RuleRow").ToList();
            for (int i = 0; i < RulesData.Count; i++)
            {
                int preDir = Convert.ToInt32(RulesData[i].Element("PreDir").Value);
                Rules.Add(new Rule
                {
                    RuleID = Convert.ToInt32(RulesData[i].Element("RuleID").Value),
                    LeftDirection = Convert.ToInt32(RulesData[i].Element("LeftDir").Value),
                    RightDirection = Convert.ToInt32(RulesData[i].Element("RightDir").Value),
                    MinDistance = Convert.ToInt32(RulesData[i].Element("MinDist").Value),
                    MaxDistance = Convert.ToInt32(RulesData[i].Element("MaxDist").Value),
                    PreemptDistance = Convert.ToInt32(RulesData[i].Element("PreDist").Value),
                    PreemptDirection = (DirectionEnum)CommonHelper.PreDirection(preDir),
                    PreemptETA = Convert.ToInt32(RulesData[i].Element("PreETA").Value),
                    MaxDuration = Convert.ToInt32(RulesData[i].Element("MaxDur").Value),
                    ClearDist = Convert.ToInt32(RulesData[i].Element("ClearDist").Value),
                    LeftIndicator = Convert.ToInt32(RulesData[i].Element("LeftInd").Value),
                    RightIndicator = Convert.ToInt32(RulesData[i].Element("RightInd").Value),
                    StraightIndicator = Convert.ToInt32(RulesData[i].Element("StrtInd").Value),
                    ETADisableSpeed = Convert.ToInt32(RulesData[i].Element("ETADisSpd").Value),
                    ETADisableSpeedMin = Convert.ToInt32(RulesData[i].Element("ETADisSpdMin").Value),
                    PClass = Convert.ToString(RulesData[i].Element("Class").Value),
                    PreemptNumber = Convert.ToInt32(RulesData[i].Element("Output").Value),
                    HeadChk = Convert.ToInt32(RulesData[i].Element("HeadChk").Value),
                });
            }

            return Rules;
        }
        public IRule AddRule(ref Device device, IRule newResult)
        {
            newResult.RuleID = device.Rules.Count + 1;
            if ((device.FixLatitude == 90000001 | device.FixLatitude == 0) && (device.FixLongitude == 180000001 | device.FixLongitude == 0))
            {
                newResult.PrepareRulePoints(device.DeviceID, newResult.RuleID, device.Latitude, device.Longitude);
            }
            else
            {
                newResult.PrepareRulePoints(device.DeviceID, newResult.RuleID, device.FixLatitude, device.FixLongitude);
            }
            device.Rules.Add(newResult);

            return newResult;
        }
    }
}
    