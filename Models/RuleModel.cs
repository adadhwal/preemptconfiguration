﻿using PreemptConfiguration.Common;
using PreemptConfiguration.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PreemptConfiguration.Models
{
    public class RuleModel
    {
        public int RuleID { get; set; }
        public int LeftDirection { get; set; }
        public int RightDirection { get; set; }
        public int MinDistance { get; set; }
        public int MinDistanceFT { get; set; }
        public int MaxDistance { get; set; }
        public int MaxDistanceFT { get; set; }
        public IPoint LeftMax { get; set; }
        public IPoint LeftMin { get; set; }
        public IPoint RightMax { get; set; }
        public IPoint RightMin { get; set; }
        public int PreemptDistance { get; set; }
        public int PreemptDistanceFT { get; set; }
        public DirectionEnum PreemptDirection { get; set; }
        public int PreemptETA { get; set; }
        public int MaxDuration { get; set; }
        public int ClearDist { get; set; }
        public int ClearDistFT { get; set; }
        public int LeftIndicator { get; set; }
        public int RightIndicator { get; set; }
        public int StraightIndicator { get; set; }
        public int ETADisableSpeed { get; set; }
        public int ETADisableSpeedMPH { get; set; }
        public int ETADisableSpeedMin { get; set; }
        public int ETADisableSpeedMinMPH { get; set; }
        public string PClass { get; set; }
        public int PreemptNumber { get; set; }
        public bool RuleChanged { get; set; }
        public int HeadChk { get; set; }

        //Methods
        public int mvID { get; set; }
        public int mvRule { get; set; }

        public int mvClass { get; set; }

        public int mvMin { get; set; }
        public int mvMax { get; set; }
        public string mvProt { get; set; }
        public int mvOut { get; set; }

    }
}