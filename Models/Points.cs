﻿using PreemptConfiguration.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace PreemptConfiguration.Models
{
    public class Points
    {
        private List<IPoint> myPoints = new List<IPoint>();

        public Points(IRule rule)
        {
            myPoints.Add(rule.LeftMin);
            myPoints.Add(rule.LeftMax);
            myPoints.Add(rule.RightMax);
            myPoints.Add(rule.RightMin);
        }

        public List<IPoint> RulePoints { get { return this.myPoints; } }
        public IEnumerator<IPoint> GetEnumerator()
        {
            int item = 0;
            // Iterate items in this list.
            foreach (IPoint pt in this.myPoints)
            {
                yield return pt;
                ++item;
            }
        }
    }
    public class Point : IPoint
    {
        public void ChangePoint(double startLat, double startLong, int newBearing, int newDistance)
        {
            try
            {


                double latStart, lonStart, distance, radDir, newLat, newLon;

                latStart = startLat * (Math.PI / 180.0);
                lonStart = startLong * (Math.PI / 180.0);

                //Distance----------------------------------------------------------------------
                distance = newDistance; //divide by earth radius km
                distance = distance / (6371.0 * 1000.0); //meters

                radDir = newBearing != 0 ? newBearing * (Math.PI / 180.0) : (Math.PI / 180.0);

                newLat = Math.Asin(Math.Sin(latStart) * Math.Cos(distance) + Math.Cos(latStart) * Math.Sin(distance) * Math.Cos(radDir));
                newLon = lonStart + Math.Atan2(Math.Sin(radDir) * Math.Sin(distance) * Math.Cos(latStart), Math.Cos(distance) - Math.Sin(latStart) * Math.Sin(newLat));


                this.BearingFromStart = newBearing;
                this.DistanceFromStart = newDistance;
                this.Latitude = newLat * (180.0 / Math.PI);
                this.Longitude = newLon * (180.0 / Math.PI);
            }
            catch (Exception ex)
            {
                //Errorlog(ex.Message.ToString() + "/" + ex.StackTrace.ToString());
            }
        }
        public void Errorlog(string msg)
        {
            string logfilepath = HttpContext.Current.Server.MapPath("~/Log.txt");
            FileStream objFilestream = new FileStream(string.Format("{0}", logfilepath), FileMode.Append, FileAccess.Write);
            StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
            objStreamWriter.WriteLine(msg);
            objStreamWriter.WriteLine("======================================================================================================");
            objStreamWriter.Close();
            objFilestream.Close();
        }
        #region IPoint Members

        public string PointIcon { get; set; }
        public string PointID { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int DistanceFromStart { get; set; }
        public int DistanceFromStartFeet { get; set; }
        public int BearingFromStart { get; set; }
        public string DistancePartnerID { get; set; }
        public string BearingPartnerID { get; set; }

        #endregion
    }
}
