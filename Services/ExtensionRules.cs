﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreemptConfiguration.Models
{
    public class ExtensionRules
    {
        public string rptID { get; set; }
        public string mvID { get; set; }
        public string mvMax { get; set; }
        public string mvMin { get; set; }
        public string mvClass { get; set; }
        public string mvRule { get; set; }
        public string mvOut { get; set; }
        public string mvProt { get; set; }
        public string mvProtOutput { get; set; }
        public List<ExtensionRules> MVechile { get; set; }
    }
}

